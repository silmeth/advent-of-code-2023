use std::collections::{HashMap, HashSet, VecDeque};

use anyhow::{anyhow, Context as _};
use smallvec::SmallVec;

pub mod parsing;

pub type Graph<'a> = HashMap<&'a str, SmallVec<[&'a str; 4]>>;
pub type Node<'a> = &'a str;
pub type Edge<'a> = (Node<'a>, Node<'a>);

struct Capacities<'a>(HashMap<Edge<'a>, bool>);

impl<'a> Capacities<'a> {
    fn has_capacity(&self, edge: Edge<'a>) -> bool {
        self.0.get(&edge).copied().unwrap_or(true)
    }

    fn flow_through(&mut self, edge: Edge<'a>) {
        // fill edge in this direction, mark the other direction as available
        self.0.insert(edge, false);
        self.0.insert((edge.1, edge.0), true);
    }
}

// breadth-first search
fn find_and_take_path<'a>(
    src: Node<'a>,
    dst: Node<'a>,
    graph: &Graph<'a>,
    capacities: &mut Capacities<'a>,
) -> bool {
    let mut visited_from = HashMap::new();
    let mut queue = VecDeque::new();

    graph[src]
        .iter()
        .filter(|n| capacities.has_capacity((src, n)))
        .for_each(|n| queue.push_back((src, *n)));
    while let Some((p, cur)) = queue.pop_front() {
        // the path found, now let’s trace back our steps and mark the flows
        if cur == dst {
            capacities.flow_through((p, cur));

            let mut cur = p;
            while let Some(p) = visited_from.get(cur) {
                capacities.flow_through((*p, cur));
                cur = p;
                if cur == src {
                    break;
                }
            }

            return true;
        }

        // path not found, let’s continue
        visited_from.insert(cur, p);
        graph[cur]
            .iter()
            .filter(|n| !visited_from.contains_key(**n) && capacities.has_capacity((cur, n)))
            .for_each(|n| queue.push_back((cur, *n)));
    }

    false
}

fn make_residual_capacities<'a>(src: Node<'a>, dst: Node<'a>, graph: &Graph<'a>) -> Capacities<'a> {
    let mut capacities = Capacities(HashMap::new());
    while find_and_take_path(src, dst, graph, &mut capacities) {}
    capacities
}

/// Returns the number of min cut edges and the size of the source residual graph.
fn min_cut<'a>(src: Node<'a>, dst: Node<'a>, graph: &Graph<'a>) -> anyhow::Result<(u32, u32)> {
    let capacities = make_residual_capacities(src, dst, graph);

    let mut visited = HashSet::new();
    let mut candidates = Vec::new();

    min_cut_rec(src, graph, &capacities, &mut visited, &mut candidates);
    let res = candidates
        .into_iter()
        .filter(|(_, n)| !visited.contains(n))
        .count();
    Ok((
        u32::try_from(res).context("res OOB")?,
        u32::try_from(visited.len()).context("visted.len() OOB")?,
    ))
}

// depth-first recursive search
fn min_cut_rec<'a>(
    cur: Node<'a>,
    graph: &Graph<'a>,
    capacities: &Capacities<'a>,
    visited: &mut HashSet<Node<'a>>,
    res: &mut Vec<Edge<'a>>,
) {
    visited.insert(cur);
    for neighbour in &graph[cur] {
        match (
            capacities.has_capacity((cur, neighbour)),
            visited.contains(neighbour),
        ) {
            // can’t be reached from here and unvisited – potentially min-cut edge
            (false, false) => res.push((cur, neighbour)),
            (true, false) => min_cut_rec(neighbour, graph, capacities, visited, res),
            (_, true) => (),
        }
    }
}

pub fn part1(graph: &Graph) -> anyhow::Result<u32> {
    let mut graph_iter = graph.iter();
    let (src, _) = graph_iter.next().context("empty graph")?;

    for (dst, _) in graph_iter {
        let (cut_size, residual_size) = min_cut(src, dst, graph)?;
        if cut_size == 3 {
            return Ok(residual_size
                * (u32::try_from(graph.keys().count()).context("OOB")? - residual_size));
        }
    }

    Err(anyhow!("min cut of size 3 not found!"))
}

pub fn part2(_graph: &Graph) -> anyhow::Result<u32> {
    todo!()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "jqt: rhn xhk nvd\n",
        "rsh: frs pzl lsr\n",
        "xhk: hfx\n",
        "cmg: qnr nvd lhk bvb\n",
        "rhn: xhk bvb hfx\n",
        "bvb: xhk hfx\n",
        "pzl: lsr hfx nvd\n",
        "qnr: nvd\n",
        "ntq: jqt hfx bvb xhk\n",
        "nvd: lhk\n",
        "lsr: lhk\n",
        "rzs: qnr cmg lsr rsh\n",
        "frs: qnr lhk lsr\n",
    );

    #[test]
    fn should_parse_input() {
        let result = parsing::parse(INPUT).unwrap();
        assert_eq!(15, result.len());
    }

    #[test]
    fn test_part1() {
        let result = parsing::parse(INPUT).unwrap();
        assert_eq!(54, part1(&result).unwrap());
    }
}
