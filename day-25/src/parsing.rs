use anyhow::Context as _;
use smallvec::SmallVec;

use crate::Graph;

pub fn parse(input: &str) -> anyhow::Result<Graph> {
    let mut res = Graph::new();

    for line in input.lines() {
        let (name, connected_to) = line
            .split_once(':')
            .with_context(|| format!("expected <str>: [list of str], got '{line}'"))?;
        let connected_to = connected_to.trim().split(' ').collect::<SmallVec<[_; 4]>>();

        for x in &connected_to {
            res.entry(x).or_default().push(name);
        }

        res.entry(name).or_default().extend(connected_to);
    }

    Ok(res)
}
