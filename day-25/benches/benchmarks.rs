use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_25::*;

static INPUT: &str = include_str!("input");

fn parse_benchmark(c: &mut Criterion) {
    c.bench_function("day 25, parsing", |b| {
        b.iter(|| parsing::parse(black_box(INPUT)).unwrap())
    });
}

fn part1_benchmark(c: &mut Criterion) {
    let graph = parsing::parse(INPUT).unwrap();
    c.bench_function("day 25, part 1", |b| {
        b.iter(|| {
            assert_eq!(562912, part1(black_box(&graph)).unwrap());
        })
    });
}

criterion_group!(benches, parse_benchmark, part1_benchmark);
criterion_main!(benches);
