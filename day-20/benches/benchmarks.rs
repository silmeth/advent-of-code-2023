use std::collections::HashMap;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_20::*;

static INPUT: &str = include_str!("input");

fn parse_benchmark(c: &mut Criterion) {
    c.bench_function("day 20, parsing", |b| {
        b.iter(|| parse(black_box(INPUT)).unwrap())
    });
}

fn part1_benchmark(c: &mut Criterion) {
    let (mut devices, _) = parse(INPUT).unwrap();
    c.bench_function("day 20, part 1", |b| {
        b.iter(|| {
            assert_eq!(861743850, part1(black_box(&mut devices)).unwrap());
            reset_all(&mut devices);
        })
    });
}

fn part2_benchmark(c: &mut Criterion) {
    let (mut devices, inputs_map) = parse(INPUT).unwrap();
    c.bench_function("day 20, part 2", |b| {
        b.iter(|| {
            assert_eq!(
                247023644760071,
                part2(black_box(&mut devices), black_box(&inputs_map)).unwrap()
            );
            reset_all(&mut devices);
        })
    });
}

criterion_group!(benches, parse_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);

fn reset_all(devices: &mut HashMap<&str, Device>) {
    for dev in devices.values_mut() {
        dev.reset()
    }
}
