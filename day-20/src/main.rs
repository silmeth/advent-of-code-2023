use day_20::{parse, part1, part2};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;
    let raw_input = &utils::read_entire_stdin()?;

    let (mut devices, dev_to_in) = parse(raw_input)?;
    run(
        part,
        |(devs, _)| part1(devs),
        |(devs, dev_to_in)| part2(devs, dev_to_in),
        (&mut devices, &dev_to_in),
    )
}
