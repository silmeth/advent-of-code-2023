use std::collections::hash_map::DefaultHasher;
use std::collections::{BTreeMap, HashMap, VecDeque};
use std::hash::{Hash, Hasher};

use anyhow::{anyhow, Context as _};
use num::Integer as _;
use parsing::parse_devices;
use smallvec::SmallVec;

pub mod parsing;

pub type ConnectionsVec<'a> = SmallVec<[&'a str; 8]>;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Device<'a> {
    name: &'a str,
    typ: DeviceType<'a>,
    outputs: ConnectionsVec<'a>,
}

impl<'a> Device<'a> {
    pub fn process_signal(
        &mut self,
        input: Pulse<'a>,
        output_queue: &mut VecDeque<(&'a str, Pulse<'a>)>,
    ) {
        match (&mut self.typ, input.state) {
            (DeviceType::FlipFlop(state), State::Low) => {
                *state = state.flip();
                Self::send(self.name, &self.outputs, *state, output_queue);
            }
            (DeviceType::Conjunction(state), _) => {
                *state.get_mut(input.sender).unwrap() = input.state;
                if state.values().all(|p| *p == State::High) {
                    Self::send(self.name, &self.outputs, State::Low, output_queue);
                } else {
                    Self::send(self.name, &self.outputs, State::High, output_queue);
                }
            }
            (DeviceType::Broadcast, recv) => {
                Self::send(self.name, &self.outputs, recv, output_queue)
            }
            _ => {}
        }
    }

    pub fn send(
        sender: &'a str,
        addresses: &[&'a str],
        state: State,
        output_queue: &mut VecDeque<(&'a str, Pulse<'a>)>,
    ) {
        for address in addresses {
            output_queue.push_back((address, Pulse { sender, state }));
        }
    }

    pub fn reset(&mut self) {
        match self.typ {
            DeviceType::FlipFlop(ref mut s) => *s = State::Low,
            DeviceType::Conjunction(ref mut inputs) => {
                for s in inputs.values_mut() {
                    *s = State::Low;
                }
            }
            DeviceType::Broadcast => (),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum DeviceType<'a> {
    FlipFlop(State),
    Conjunction(BTreeMap<&'a str, State>),
    Broadcast,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct Pulse<'a> {
    sender: &'a str,
    state: State,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum State {
    High,
    Low,
}

impl State {
    fn flip(&self) -> Self {
        match self {
            State::High => State::Low,
            State::Low => State::High,
        }
    }
}

type Devices<'a> = HashMap<&'a str, Device<'a>>;
type DevToInputs<'a> = HashMap<&'a str, ConnectionsVec<'a>>;

pub fn parse(input: &str) -> anyhow::Result<(Devices, DevToInputs)> {
    let (_, devices) = nom::combinator::all_consuming(parse_devices)(input)
        .map_err(|e| anyhow!("{}", e.map(|e| nom::error::convert_error(input, e))))?;

    let dev_to_inputs: HashMap<&str, ConnectionsVec> = devices
        .iter()
        .flat_map(|dev| dev.outputs.iter().map(|out| (*out, dev.name)))
        .fold(HashMap::new(), |mut map, (dev, input)| {
            map.entry(dev).or_default().push(input);
            map
        });

    // set initial Conjunction inputs
    let devices = devices
        .into_iter()
        .map(|mut dev| {
            if let DeviceType::Conjunction(map) = &mut dev.typ {
                dev_to_inputs
                    .get(dev.name)
                    .with_context(|| format!("no input for &{}", dev.name))?
                    .iter()
                    .for_each(|input| {
                        map.insert(input, State::Low);
                    });
            }
            Ok((dev.name, dev))
        })
        .collect::<anyhow::Result<_>>()?;
    Ok((devices, dev_to_inputs))
}

fn send_button_signal(signals_queue: &mut VecDeque<(&str, Pulse)>) {
    signals_queue.push_back((
        "broadcaster",
        Pulse {
            sender: "button",
            state: State::Low,
        },
    ))
}

pub fn part1<'b, 'a: 'b>(devices: &'b mut HashMap<&'a str, Device<'a>>) -> anyhow::Result<u64> {
    let mut low_count = 0;
    let mut high_count = 0;

    let mut signals_queue = VecDeque::new();

    // TODO: optimize by cycle detection
    for _ in 0..1000 {
        send_button_signal(&mut signals_queue);

        while let Some((address, signal)) = signals_queue.pop_front() {
            match signal.state {
                State::Low => low_count += 1,
                State::High => high_count += 1,
            }

            // we ignore signals sent to unknown addresses (output, rx…)
            if let Some(addressee) = devices.get_mut(address) {
                addressee.process_signal(signal, &mut signals_queue);
            }
        }
    }

    Ok(low_count * high_count)
}

pub fn part2<'a, 'b, 'c>(
    devices: &'b mut HashMap<&'a str, Device<'a>>,
    dev_to_inputs: &'c DevToInputs<'a>,
) -> anyhow::Result<u64>
where
    'a: 'b,
    'a: 'c,
{
    let mut signals_queue = VecDeque::new();

    // let’s assume that there is a single Conjunction node outputting to `rx`
    // and that it has multiple inputs, and that each input has a long repeat-state cycle
    // and that the lengths of those cycles have few and large common divisors – thus
    // it’s effectively a very large common multiplier that makes it take so long for `rx`
    // to receive a LOW signal. So let’s monitor the system enough to detect the cycles in the
    // inputs to that single module and hope the least common multiple of those cycles is the
    // correct number.
    //
    // Inspired by https://www.ericburden.work/blog/2023/12/20/advent-of-code-day-20/

    let outputting_to_rx = dev_to_inputs.get("rx").context("device 'rx' not found")?;
    if outputting_to_rx.len() != 1 {
        return Err(anyhow!(
            "assumption broken, more than 1 devices write to 'rx'; len = {}",
            outputting_to_rx.len()
        ));
    }
    let outputting_to_rx = outputting_to_rx[0];

    match devices
        .get(outputting_to_rx)
        .with_context(|| format!("device '{outputting_to_rx}' (rx’s input) not found"))?
        .typ
    {
        DeviceType::Conjunction(_) => (),
        ref typ => {
            return Err(anyhow!(
                "assumption broken: the thing outputting to 'rx' is not a conjunction: {typ:?}"
            ))
        }
    };

    let mut cycle_history: HashMap<&str, HashMap<_, _>> = dev_to_inputs[outputting_to_rx]
        .iter()
        .map(|dev_id| (*dev_id, HashMap::<u64, u64>::new()))
        .collect();
    let mut cycles_found: HashMap<&str, u64> = HashMap::with_capacity(outputting_to_rx.len());

    'outer: for i in 0.. {
        send_button_signal(&mut signals_queue);

        while let Some((address, signal)) = signals_queue.pop_front() {
            if address == "rx" {
                if signal.state == State::Low {
                    return Ok(i);
                } else {
                    continue;
                }
            }

            if let (Some(monitored), State::High) =
                (cycle_history.get_mut(signal.sender), signal.state)
            {
                let monitored_device = &devices[signal.sender];
                let mut hasher = DefaultHasher::new();
                monitored_device.typ.hash(&mut hasher);
                let state_hash = hasher.finish();

                if let Some(cycle_iteration) = monitored.get(&state_hash) {
                    cycles_found
                        .entry(signal.sender)
                        .or_insert(i - cycle_iteration);
                } else {
                    monitored.insert(state_hash, i);
                }
            }

            if cycles_found.len() == cycle_history.len() {
                // found all needed input cycles – we’re done
                break 'outer;
            }

            let addressee = devices
                .get_mut(address)
                .with_context(|| format!("unknown device '{address}'"))?;

            addressee.process_signal(signal, &mut signals_queue);
        }
    }

    // lowest common multiple of the cycles is the answer
    Ok(cycles_found
        .iter()
        .fold(1, |acc, (_, cycle)| acc.lcm(cycle)))
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "broadcaster -> a\n",
        "%a -> inv, con\n",
        "&inv -> b\n",
        "%b -> con\n",
        "&con -> output\n",
    );

    #[test]
    fn should_parse_input() {
        let devices = parse(INPUT).unwrap();
        assert_eq!(5, devices.0.len());
    }

    #[test]
    fn test_part1() {
        let (mut devices, _) = parse(INPUT).unwrap();
        assert_eq!(11687500, part1(&mut devices).unwrap());
    }
}
