use std::collections::BTreeMap;

use nom::{
    branch::alt,
    bytes::complete::{tag, take, take_while},
    character::complete::alpha1,
    combinator::opt,
    error::{context, ErrorKind, ParseError as _, VerboseError},
    multi::separated_list1,
    IResult,
};

use crate::{ConnectionsVec, Device, DeviceType, State};

type ParseResult<'a, T> = IResult<&'a str, T, VerboseError<&'a str>>;

pub fn parse_devices(input: &str) -> ParseResult<Vec<Device>> {
    let (input, devs) = separated_list1(newline, device)(input)?;
    let (input, _) = opt(newline)(input)?;

    Ok((input, devs))
}

fn device(input: &str) -> ParseResult<Device> {
    let (input, d_type) = context("device type ('%', '&', or 'broadcaster')", device_type)(input)?;
    let (input, name) = match &d_type {
        DeviceType::FlipFlop(_) | DeviceType::Conjunction(_) => name(input)?,
        // already parsed out
        DeviceType::Broadcast => (input, "broadcaster"),
    };

    let (input, _) = whitespace(input)?;
    let (input, _) = tag("->")(input)?;
    let (input, _) = whitespace(input)?;
    let (input, outputs) = context("outputs list", outputs)(input)?;
    Ok((
        input,
        Device {
            name,
            typ: d_type,
            outputs,
        },
    ))
}

fn device_type(input: &str) -> ParseResult<DeviceType> {
    let (remaining, d_tag) = take(1usize)(input)?;
    match d_tag {
        "%" => Ok((remaining, DeviceType::FlipFlop(State::Low))),
        // the inputs hashmap will be filled later
        "&" => Ok((remaining, DeviceType::Conjunction(BTreeMap::new()))),
        "b" => {
            let (remaining, _) = tag("broadcaster")(input)?;
            Ok((remaining, DeviceType::Broadcast))
        }
        _ => Err(nom::Err::Error(VerboseError::from_error_kind(
            input,
            ErrorKind::Tag,
        ))),
    }
}

fn outputs(mut input: &str) -> ParseResult<ConnectionsVec> {
    let mut res = ConnectionsVec::new();

    loop {
        let d_name;
        (input, d_name) = name(input)?;
        res.push(d_name);

        match tag(",")(input) {
            Ok((remaining, _)) => {
                input = remaining;
                (input, _) = whitespace(input)?;
            }
            Err(nom::Err::Error(_)) => break,
            Err(e) => return Err(e),
        }
    }

    Ok((input, res))
}

fn name(input: &str) -> ParseResult<'_, &str> {
    context("device name", alpha1)(input)
}

fn whitespace(i: &str) -> ParseResult<'_, &str> {
    context("whitespace", take_while(|c| " \t".contains(c)))(i)
}

fn newline(i: &str) -> ParseResult<'_, &str> {
    context("new line", alt((tag("\n"), tag("\r\n"))))(i)
}
