use nalgebra::{vector, Vector3};
use nom::{
    branch::alt,
    bytes::complete::{tag, take_while},
    character::complete,
    error::context,
    multi::separated_list1,
    IResult,
};

use crate::Hailstone;

type ParseResult<'a, T> = IResult<&'a str, T, nom::error::VerboseError<&'a str>>;

pub fn parse_all(input: &str) -> ParseResult<Vec<Hailstone>> {
    separated_list1(newline, hailstone)(input)
}

fn newline(input: &str) -> ParseResult<&str> {
    context("newline", alt((tag("\n"), tag("\r\n"))))(input)
}

fn whitespace(input: &str) -> ParseResult<&str> {
    context("whitespace", take_while(|c| " \t".contains(c)))(input)
}

fn hailstone(input: &str) -> ParseResult<Hailstone> {
    let (input, xyz) = point(input)?;
    let (input, _) = whitespace(input)?;
    let (input, _) = tag("@")(input)?;
    let (input, _) = whitespace(input)?;
    let (input, v) = point(input)?;

    Ok((
        input,
        Hailstone {
            position: xyz,
            velocity: v,
        },
    ))
}

fn point(input: &str) -> ParseResult<Vector3<f64>> {
    let (input, x) = complete::i64(input)?;
    let (input, _) = tag(",")(input)?;
    let (input, _) = whitespace(input)?;
    let (input, y) = complete::i64(input)?;
    let (input, _) = tag(",")(input)?;
    let (input, _) = whitespace(input)?;
    let (input, z) = complete::i64(input)?;
    Ok((input, vector![x as f64, y as f64, z as f64]))
}
