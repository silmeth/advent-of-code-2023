use day_24::{parse, part1, part2};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let hailstones = parse(&utils::read_entire_stdin()?)?;

    run(
        part,
        |h| Ok(part1(h, (200000000000000, 400000000000000))),
        part2,
        &hailstones,
    )
}
