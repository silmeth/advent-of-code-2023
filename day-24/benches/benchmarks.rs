use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_24::*;

static INPUT: &str = include_str!("input");

fn parse_benchmark(c: &mut Criterion) {
    c.bench_function("day 24, parsing", |b| {
        b.iter(|| parse(black_box(INPUT)).unwrap())
    });
}

fn part1_benchmark(c: &mut Criterion) {
    let graph = parse(INPUT).unwrap();
    c.bench_function("day 24, part 1", |b| {
        b.iter(|| {
            assert_eq!(
                16502,
                part1(black_box(&graph), (200000000000000, 400000000000000))
            );
        })
    });
}

fn part2_benchmark(c: &mut Criterion) {
    let graph = parse(INPUT).unwrap();
    c.bench_function("day 24, part 2", |b| {
        b.iter(|| {
            assert_eq!(673641951253289, part2(black_box(&graph)).unwrap());
        })
    });
}

criterion_group!(benches, parse_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);
