use anyhow::anyhow;
use anyhow::Context as _;
use std::cmp::Reverse;
use std::collections::HashMap;
use std::io::BufRead as _;
use std::{mem::MaybeUninit, str::FromStr};

use utils::run;

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord, Clone, Copy, Hash)]
enum Card {
    Num(u8),
    T,
    J,
    Q,
    K,
    A,
}

impl Card {
    fn rank(&self) -> usize {
        match self {
            Self::Num(num) => (*num as usize) - 2,
            Self::T => 8,
            Self::J => 9,
            Self::Q => 10,
            Self::K => 11,
            Self::A => 12,
        }
    }

    fn rank2(&self) -> usize {
        match self {
            Self::J => 0,
            Self::Num(num) => (*num as usize) - 1,
            Self::T => 9,
            Self::Q => 10,
            Self::K => 11,
            Self::A => 12,
        }
    }
}

impl TryFrom<char> for Card {
    type Error = anyhow::Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '0'..='9' => Ok(Self::Num(value.to_digit(10).unwrap().try_into().unwrap())),
            'T' => Ok(Self::T),
            'J' => Ok(Self::J),
            'Q' => Ok(Self::Q),
            'K' => Ok(Self::K),
            'A' => Ok(Self::A),
            _ => Err(anyhow!("Unknown card type {}", value)),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
enum HandType {
    High(Card),
    OnePair(Card),
    TwoPair(Card, Card),
    ThreeOfKind(Card),
    FullHouse(Card, Card),
    FourOfKind(Card),
    FiveOfKind(Card),
}

impl HandType {
    fn rank(&self) -> u64 {
        match self {
            Self::High(_) => 0,
            Self::OnePair(_) => 1,
            Self::TwoPair(_, _) => 2,
            Self::ThreeOfKind(_) => 3,
            Self::FullHouse(_, _) => 4,
            Self::FourOfKind(_) => 5,
            Self::FiveOfKind(_) => 6,
        }
    }

    fn from_2nd_ruleset(cards: &[Card; 5]) -> Self {
        let mut counts = [0; 13];
        let mut repeats: Vec<(Card, u64)> = cards
            .iter()
            .map(|card| {
                counts[card.rank2()] += 1;
                (*card, counts[card.rank2()])
            })
            .filter(|(_, count)| *count >= 2)
            .filter(|(card, _)| card != &Card::J)
            // make unique
            .collect::<HashMap<Card, u64>>()
            .into_iter()
            .collect();

        let jokers = counts[0];

        if jokers == 5 {
            return Self::FiveOfKind(Card::A);
        }

        repeats.sort_by_key(|(card, count)| Reverse((*count, card.rank())));
        if !repeats.is_empty() {
            repeats[0].1 += jokers;
        } else {
            repeats.push((
                cards.iter().cloned().max_by_key(Card::rank2).unwrap(),
                jokers + 1,
            ))
        }

        match repeats.len() {
            2 => match (repeats[0], repeats[1]) {
                ((card1, 3), (card2, 2)) => Self::FullHouse(card1, card2),
                ((card1, 2), (card2, 2)) => Self::TwoPair(card1, card2),
                _ => panic!("{repeats:?}"),
            },
            1 => match repeats[0] {
                (card, 5) => Self::FiveOfKind(card),
                (card, 4) => Self::FourOfKind(card),
                (card, 3) => Self::ThreeOfKind(card),
                (card, 2) => Self::OnePair(card),
                (card, 1) => Self::High(card),
                _ => panic!("{repeats:?}"),
            },
            _ => Self::High(*cards.iter().max_by_key(|card| card.rank()).unwrap()),
        }
    }
}

impl From<&[Card; 5]> for HandType {
    fn from(cards: &[Card; 5]) -> Self {
        let mut counts = [0; 13];
        let mut repeats: Vec<(Card, u64)> = cards
            .iter()
            .map(|card| {
                counts[card.rank()] += 1;
                (*card, counts[card.rank()])
            })
            .filter(|(_, count)| *count >= 2)
            // make unique
            .collect::<HashMap<Card, u64>>()
            .into_iter()
            .collect();

        repeats.sort_by_key(|(card, count)| Reverse((*count, card.rank())));

        match repeats.len() {
            2 => match (repeats[0], repeats[1]) {
                ((card1, 3), (card2, 2)) => Self::FullHouse(card1, card2),
                ((card1, 2), (card2, 2)) => Self::TwoPair(card1, card2),
                _ => unreachable!(),
            },
            1 => match repeats[0] {
                (card, 5) => Self::FiveOfKind(card),
                (card, 4) => Self::FourOfKind(card),
                (card, 3) => Self::ThreeOfKind(card),
                (card, 2) => Self::OnePair(card),
                _ => unreachable!(),
            },
            _ => Self::High(*cards.iter().max_by_key(|card| card.rank()).unwrap()),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
struct Hand {
    cards: [Card; 5],
    bid: u64,
}

impl Hand {
    fn typ(&self) -> HandType {
        HandType::from(&self.cards)
    }

    fn type_2nd_ruleset(&self) -> HandType {
        HandType::from_2nd_ruleset(&self.cards)
    }
}

impl FromStr for Hand {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (hand, bid) = s.split_once(' ').context("")?;
        // SAFETY: the type we’re assuming is initialized (`[MaybeUninit<_>; 5]`) is itself
        // a block of uninitialized memory thus `assume_init()` is always correct on it.
        let mut cards: [MaybeUninit<_>; 5] = unsafe { MaybeUninit::uninit().assume_init() };
        for (i, card) in hand.chars().map(Card::try_from).enumerate() {
            if i >= 5 {
                return Err(anyhow!("Too many cards!"));
            }
            cards[i].write(card?);
        }

        // SAFETY: we’ve successfully initialized the array
        let cards = unsafe { std::mem::transmute(cards) };

        let bid = bid.parse().context("bid is not a number")?;

        Ok(Self { cards, bid })
    }
}

fn part1<I: Iterator<Item = anyhow::Result<Hand>>>(hands: I) -> anyhow::Result<u64> {
    let mut hands: Vec<Hand> = hands.collect::<anyhow::Result<_>>()?;
    hands.sort_by_key(|hand| (hand.typ().rank(), hand.cards));
    Ok(hands
        .iter()
        .enumerate()
        .map(|(rank, hand)| (rank as u64 + 1) * hand.bid)
        .sum())
}

fn part2<I: Iterator<Item = anyhow::Result<Hand>>>(hands: I) -> anyhow::Result<u64> {
    let mut hands: Vec<Hand> = hands.collect::<anyhow::Result<_>>()?;
    hands.sort_by_key(|hand| {
        (
            hand.type_2nd_ruleset().rank(),
            hand.cards.iter().map(Card::rank2).collect::<Vec<_>>(),
        )
    });
    Ok(hands
        .iter()
        .enumerate()
        .map(|(rank, hand)| (rank as u64 + 1) * hand.bid)
        .sum())
}

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let stdin = std::io::stdin().lock();
    let hands = stdin.lines().map(|line| {
        line.context("Failed to read line")
            .and_then(|line| line.parse::<Hand>())
    });

    run(part, part1, part2, hands)
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = concat!(
        "32T3K 765\n",
        "T55J5 684\n",
        "KK677 28\n",
        "KTJJT 220\n",
        "QQQJA 483\n",
    );

    #[test]
    fn example_input_part1() {
        let result = part1(TEST_INPUT.lines().map(str::parse::<Hand>)).unwrap();
        assert_eq!(6440, result);
    }

    #[test]
    fn example_input_part2() {
        let result = part2(TEST_INPUT.lines().map(str::parse::<Hand>)).unwrap();
        assert_eq!(5905, result);
    }
}
