use std::str::FromStr;

use anyhow::{anyhow, Context as _};

#[derive(Debug)]
pub struct Pattern {
    map: Vec<Plot>,
    width: usize,
    height: usize,
}

impl Pattern {
    fn find_horizontal_symmetry_axis(&self, smudges: usize) -> Option<u32> {
        (1..self.height)
            .filter(|y_sym| {
                let bound = usize::min(y_sym * 2, self.height);
                smudges
                    == (*y_sym..bound)
                        .map(|y_check| {
                            let below = &self[y_check];
                            let above = &self[2 * y_sym - y_check - 1];
                            below
                                .iter()
                                .zip(above.iter())
                                .filter(|(below, above)| below != above)
                                .count()
                        })
                        .sum()
            })
            .map(|y_sym| y_sym as u32)
            .next()
    }

    fn find_vertical_symmetry_axis(&self, smudges: usize) -> Option<u32> {
        (1..self.width)
            .filter(|x_sym| {
                let bound = usize::min(x_sym * 2, self.width);
                smudges
                    == (*x_sym..bound)
                        .map(|x_check| {
                            (0..self.height)
                                .filter(|y| {
                                    let left = self[(x_check, *y)];
                                    let right = self[(2 * x_sym - x_check - 1, *y)];
                                    left != right
                                })
                                .count()
                        })
                        .sum()
            })
            .map(|x_sym| x_sym as u32)
            .next()
    }
}

impl std::ops::Index<(usize, usize)> for Pattern {
    type Output = Plot;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        assert!(x < self.width);
        assert!(y < self.height);

        let idx = y * self.width + x;
        &self.map[idx]
    }
}

/// Returns a full horizontal line
impl std::ops::Index<usize> for Pattern {
    type Output = [Plot];

    fn index(&self, y: usize) -> &Self::Output {
        assert!(y < self.height);

        let idx = y * self.width;
        &self.map[idx..idx + self.width]
    }
}

impl FromStr for Pattern {
    type Err = anyhow::Error;

    fn from_str(block: &str) -> Result<Self, Self::Err> {
        let width = block
            .lines()
            .next()
            .context("block must have at least one line")?
            .len();
        let (height, map) = block
            .lines()
            .map(|line| {
                if line.len() == width {
                    Ok(line)
                } else {
                    Err(anyhow!("pattern must be a rectangle, got: {:?}", block))
                }
            })
            .try_fold(
                (0, Vec::with_capacity((width * width * 2) / 3)),
                |(height_acc, mut map), line| {
                    line.and_then(move |line| {
                        for plot in line.chars().map(Plot::try_from) {
                            map.push(plot?);
                        }
                        Ok((height_acc + 1, map))
                    })
                },
            )?;

        Ok(Self { map, width, height })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Plot {
    Ash,
    Rock,
}

impl TryFrom<char> for Plot {
    type Error = anyhow::Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(Self::Ash),
            '#' => Ok(Self::Rock),
            _ => Err(anyhow!("unrecognized character '{}'", value)),
        }
    }
}

pub fn parse(s: &str) -> anyhow::Result<Vec<Pattern>> {
    let pattern_blocks = s.split("\n\n").flat_map(|s| s.split("\r\n\r\n"));
    pattern_blocks
        .map(str::parse)
        .collect::<anyhow::Result<_>>()
}

pub fn part1(patterns: &[Pattern]) -> anyhow::Result<u32> {
    inner(patterns, 0)
}

pub fn part2(patterns: &[Pattern]) -> anyhow::Result<u32> {
    inner(patterns, 1)
}

pub fn inner(patterns: &[Pattern], smudges: usize) -> anyhow::Result<u32> {
    patterns
        .iter()
        .map(|pattern| {
            pattern
                .find_vertical_symmetry_axis(smudges)
                .or_else(|| {
                    pattern
                        .find_horizontal_symmetry_axis(smudges)
                        .map(|horizontal| horizontal * 100)
                })
                .with_context(|| format!("no symmetry found in pattern: {:?}", pattern))
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "#.##..##.\n",
        "..#.##.#.\n",
        "##......#\n",
        "##......#\n",
        "..#.##.#.\n",
        "..##..##.\n",
        "#.#.##.#.\n",
        "\n",
        "#...##..#\n",
        "#....#..#\n",
        "..##..###\n",
        "#####.##.\n",
        "#####.##.\n",
        "..##..###\n",
        "#....#..#\n",
    );

    #[test]
    fn test_part1() {
        let patterns = parse(INPUT).unwrap();
        let res = part1(&patterns).unwrap();
        assert_eq!(405, res);
    }

    #[test]
    fn test_part2() {
        let patterns = parse(INPUT).unwrap();
        let res = part2(&patterns).unwrap();
        assert_eq!(400, res);
    }
}
