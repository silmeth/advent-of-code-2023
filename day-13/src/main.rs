use day_13::{parse, part1, part2};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let input = utils::read_entire_stdin()?;
    let patterns = parse(&input)?;

    run(part, part1, part2, &patterns)
}
