use anyhow::anyhow;
use anyhow::Context as _;
use std::fmt::Debug;
use std::fmt::Display;
use std::io::Read as _;

pub enum Part {
    First,
    Second,
}

impl TryFrom<&'_ str> for Part {
    type Error = anyhow::Error;

    fn try_from(value: &'_ str) -> Result<Self, Self::Error> {
        match value {
            "1" => Ok(Self::First),
            "2" => Ok(Self::Second),
            _ => Err(anyhow!("part must be `1` or `2`")),
        }
    }
}

impl TryFrom<std::env::Args> for Part {
    type Error = anyhow::Error;

    fn try_from(mut args: std::env::Args) -> Result<Self, Self::Error> {
        args.nth(1)
            .context("provide part (`1` or `2`) of the solution to run")?
            .as_str()
            .try_into()
    }
}

pub fn run<Input, Out, P1, P2>(part: Part, part1: P1, part2: P2, input: Input) -> anyhow::Result<()>
where
    P1: FnOnce(Input) -> anyhow::Result<Out>,
    P2: FnOnce(Input) -> anyhow::Result<Out>,
    Out: Display,
{
    let res = match part {
        Part::First => part1(input)?,
        Part::Second => part2(input)?,
    };

    println!("{res}");
    Ok(())
}

pub fn expected_err(expected: &str, got: &str) -> anyhow::Error {
    anyhow!(r#"expected "{expected}" got "{got}""#)
}

pub fn expect<'a>(expected: &'_ str, input: &'a str) -> anyhow::Result<&'a str> {
    if let Some(stripped) = input.strip_prefix(expected) {
        Ok(stripped)
    } else {
        Err(expected_err(expected, input))
    }
}

pub fn expect_ascii_byte(expected: u8, input: &[u8]) -> anyhow::Result<&[u8]> {
    let value = input[0];
    if value != expected {
        return Err(anyhow!(
            "expected '{}', got '{}'",
            std::str::from_utf8(&[expected]).unwrap(),
            std::str::from_utf8(&[value]).with_context(|| format!("non-ASCII byte '{value}'"))?
        ));
    }

    Ok(&input[1..])
}

pub fn read_entire_stdin() -> anyhow::Result<String> {
    let mut stdin = std::io::stdin().lock();
    let mut buf = String::new();
    stdin
        .read_to_string(&mut buf)
        .context("failed to read sky map")?;
    Ok(buf)
}

pub fn ascii_digit_to_num(b: u8) -> u8 {
    b - b'0'
}

#[allow(clippy::len_without_is_empty)]
pub trait RangeExt {
    fn intersect(&self, other: &Self) -> Self;
    fn merge(&self, other: &Self) -> Self;
    fn len(&self) -> u64;
}

impl<T> RangeExt for std::ops::Range<T>
where
    T: Ord + Copy + std::ops::Sub,
    u64: TryFrom<<T as std::ops::Sub>::Output>,
    <u64 as TryFrom<<T as std::ops::Sub>::Output>>::Error: Debug,
{
    fn intersect(&self, other: &Self) -> Self {
        std::cmp::max(self.start, other.start)..std::cmp::min(self.end, other.end)
    }

    fn merge(&self, other: &Self) -> Self {
        std::cmp::min(self.start, other.start)..std::cmp::max(self.end, other.end)
    }

    fn len(&self) -> u64 {
        if !self.is_empty() {
            u64::try_from(self.end - self.start).expect("out of bounds")
        } else {
            0
        }
    }
}

impl<T> RangeExt for std::ops::RangeInclusive<T>
where
    T: Ord + Copy + std::ops::Sub,
    u64: TryFrom<<T as std::ops::Sub>::Output>,
    <u64 as TryFrom<<T as std::ops::Sub>::Output>>::Error: Debug,
{
    fn intersect(&self, other: &Self) -> Self {
        std::cmp::max(*self.start(), *other.start())..=std::cmp::min(*self.end(), *other.end())
    }

    fn merge(&self, other: &Self) -> Self {
        std::cmp::min(*self.start(), *other.start())..=std::cmp::max(*self.end(), *other.end())
    }

    fn len(&self) -> u64 {
        if !self.is_empty() {
            1 + u64::try_from(*self.end() - *self.start()).expect("out of range")
        } else {
            0
        }
    }
}
