use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_19::parsing::*;
use day_19::*;

static INPUT: &str = include_str!("input");

fn parse_benchmark(c: &mut Criterion) {
    c.bench_function("day 19, parsing", |b| {
        b.iter(|| parse(black_box(INPUT)).unwrap())
    });
}

fn part1_benchmark(c: &mut Criterion) {
    let (workflows, parts) = parse(INPUT).unwrap();
    c.bench_function("day 19, part 1", |b| {
        b.iter(|| {
            assert_eq!(
                487623,
                part1(black_box(&workflows), black_box(&parts)).unwrap()
            )
        })
    });
}

fn part2_benchmark(c: &mut Criterion) {
    let (workflows, _) = parse(INPUT).unwrap();
    c.bench_function("day 19, part 2", |b| {
        b.iter(|| assert_eq!(113550238315130, part2(black_box(&workflows)).unwrap()))
    });
}

criterion_group!(benches, parse_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);
