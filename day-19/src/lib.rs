pub mod parsing;

use std::{collections::HashMap, ops::Range};

use anyhow::Context as _;
use smallvec::SmallVec;
use utils::RangeExt;

type Workflow<'a> = SmallVec<[Rule<'a>; 4]>;

pub struct Workflows<'a> {
    inner: HashMap<&'a str, Workflow<'a>>,
}

type ConditionFn = Box<dyn Fn(&'_ Part) -> bool>;

struct Condition {
    execute: ConditionFn,
    description: Option<ConditionDescription>,
}

#[derive(Debug)]
struct ConditionDescription {
    accepted_range: std::ops::Range<u64>,
    field: Field,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Field {
    CoolLook,
    Musical,
    Aerodynamic,
    Shiny,
}

pub struct Rule<'a> {
    condition: Condition,
    action: Action<'a>,
}

#[derive(PartialEq, Eq)]
enum Action<'a> {
    SendTo(&'a str),
    Accept,
    Reject,
}

impl<'a> Action<'a> {
    fn from_string(s: &'a str) -> Self {
        match s {
            "A" => Self::Accept,
            "R" => Self::Reject,
            name => Self::SendTo(name),
        }
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct Part {
    cool_look: u64,
    musical: u64,
    aerodynamic: u64,
    shiny: u64,
}

pub fn part1(workflows: &Workflows, parts: &[Part]) -> anyhow::Result<u64> {
    let initial_workflow = workflows.inner.get("in").context("no 'in' workflow!")?;
    parts
        .iter()
        .map(|part| {
            let mut cur_workflow = initial_workflow;
            let mut name = "in";
            loop {
                let rule = cur_workflow
                    .iter()
                    .find(|workflow| (workflow.condition.execute)(part))
                    .with_context(|| {
                        format!("no condition matching {part:?} in workflow {name}")
                    })?;

                match rule.action {
                    Action::Accept => break Ok(Some(part)),
                    Action::Reject => break Ok(None),
                    Action::SendTo(next) => {
                        (name, cur_workflow) = (
                            next,
                            workflows
                                .inner
                                .get(next)
                                .with_context(|| format!("unknown workflow {next}"))?,
                        )
                    }
                }
            }
        })
        .filter_map(|res_opt| match res_opt {
            Ok(Some(res)) => Some(Ok(res)),
            Err(e) => Some(Err(e)),
            _ => None,
        })
        .map(|res| res.map(|part| part.cool_look + part.musical + part.aerodynamic + part.shiny))
        .sum()
}

#[derive(Debug, Clone)]
struct PartFilter {
    cool_look: Range<u64>,
    musical: Range<u64>,
    aerodynamic: Range<u64>,
    shiny: Range<u64>,
}

impl Default for PartFilter {
    fn default() -> Self {
        Self {
            cool_look: 1..4001,
            musical: 1..4001,
            aerodynamic: 1..4001,
            shiny: 1..4001,
        }
    }
}

pub fn part2(workflows: &Workflows) -> anyhow::Result<u64> {
    let res = get_all_allowed_filters(workflows)?
        .into_iter()
        .map(|filter| filter.combinations())
        .sum();
    Ok(res)
}

impl PartFilter {
    fn split_out_by_condition(&self, condition: &ConditionDescription) -> (Self, [Self; 2]) {
        let (new, before, after) = match condition.field {
            Field::CoolLook => {
                let new = Self {
                    cool_look: self.cool_look.intersect(&condition.accepted_range),
                    ..self.clone()
                };
                let b = Self {
                    cool_look: Self::before(&self.cool_look, &condition.accepted_range),
                    ..self.clone()
                };
                let a = Self {
                    cool_look: Self::after(&self.cool_look, &condition.accepted_range),
                    ..self.clone()
                };
                (new, b, a)
            }
            Field::Musical => {
                let new = Self {
                    musical: self.musical.intersect(&condition.accepted_range),
                    ..self.clone()
                };
                let b = Self {
                    musical: Self::before(&self.musical, &condition.accepted_range),
                    ..self.clone()
                };
                let a = Self {
                    musical: Self::after(&self.musical, &condition.accepted_range),
                    ..self.clone()
                };
                (new, b, a)
            }
            Field::Aerodynamic => {
                let new = Self {
                    aerodynamic: self.aerodynamic.intersect(&condition.accepted_range),
                    ..self.clone()
                };
                let b = Self {
                    aerodynamic: Self::before(&self.aerodynamic, &condition.accepted_range),
                    ..self.clone()
                };
                let a = Self {
                    aerodynamic: Self::after(&self.aerodynamic, &condition.accepted_range),
                    ..self.clone()
                };
                (new, b, a)
            }
            Field::Shiny => {
                let new = Self {
                    shiny: self.shiny.intersect(&condition.accepted_range),
                    ..self.clone()
                };
                let b = Self {
                    shiny: Self::before(&self.shiny, &condition.accepted_range),
                    ..self.clone()
                };
                let a = Self {
                    shiny: Self::after(&self.shiny, &condition.accepted_range),
                    ..self.clone()
                };
                (new, b, a)
            }
        };

        (new, [before, after])
    }

    fn combinations(&self) -> u64 {
        self.cool_look.len() * self.musical.len() * self.aerodynamic.len() * self.shiny.len()
    }

    fn is_empty(&self) -> bool {
        self.cool_look.is_empty()
            || self.musical.is_empty()
            || self.aerodynamic.is_empty()
            || self.shiny.is_empty()
    }

    fn before(this: &Range<u64>, other: &Range<u64>) -> Range<u64> {
        this.start..u64::min(this.end, other.start)
    }

    fn after(this: &Range<u64>, other: &Range<u64>) -> Range<u64> {
        u64::max(other.end, this.start)..this.end
    }
}

#[inline]
fn get_all_allowed_filters(workflows: &Workflows) -> anyhow::Result<Vec<PartFilter>> {
    let mut res = Vec::new();
    add_all_allowed_filters_recursively(workflows, "in", vec![PartFilter::default()], &mut res)?;
    Ok(res)
}

fn add_all_allowed_filters_recursively(
    workflows: &Workflows,
    workflow_name: &str,
    cur_filters: Vec<PartFilter>,
    collector: &mut Vec<PartFilter>,
) -> anyhow::Result<()> {
    let mut remaining_filters = cur_filters;
    let cur_workflow = workflows
        .inner
        .get(workflow_name)
        .with_context(|| format!("workflow '{workflow_name}' not found!"))?;

    #[inline]
    fn process_action(
        workflows: &Workflows,
        action: &Action<'_>,
        mut cur_filters: Vec<PartFilter>,
        collector: &mut Vec<PartFilter>,
    ) -> anyhow::Result<()> {
        match action {
            Action::Accept => {
                collector.append(&mut cur_filters);
            }
            Action::SendTo(next) => {
                add_all_allowed_filters_recursively(workflows, next, cur_filters, collector)?;
            }
            Action::Reject => {
                // do not collect – all is rejected; remaining_filters are still valid.
            }
        }
        Ok(())
    }

    for rule in cur_workflow {
        if let Some(condition) = &rule.condition.description {
            let mut tmp_remaining = Vec::new();
            let mut tmp_collected = Vec::new();
            for filter in remaining_filters.drain(0..) {
                let (new, remaining) = filter.split_out_by_condition(condition);
                if !new.is_empty() {
                    tmp_collected.push(new);
                }
                tmp_remaining.extend(remaining.into_iter().filter(|it| !it.is_empty()));
            }
            std::mem::swap(&mut remaining_filters, &mut tmp_remaining);

            process_action(workflows, &rule.action, tmp_collected, collector)?;
        } else {
            // no condition – means this is the last rule to be reached
            process_action(workflows, &rule.action, remaining_filters, collector)?;
            break;
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "px{a<2006:qkq,m>2090:A,rfg}\n",
        "pv{a>1716:R,A}\n",
        "lnx{m>1548:A,A}\n",
        "rfg{s<537:gd,x>2440:R,A}\n",
        "qs{s>3448:A,lnx}\n",
        "qkq{x<1416:A,crn}\n",
        "crn{x>2662:A,R}\n",
        "in{s<1351:px,qqz}\n",
        "qqz{s>2770:qs,m<1801:hdj,R}\n",
        "gd{a>3333:R,R}\n",
        "hdj{m>838:A,pv}\n",
        "\n",
        "{x=787,m=2655,a=1222,s=2876}\n",
        "{x=1679,m=44,a=2067,s=496}\n",
        "{x=2036,m=264,a=79,s=2244}\n",
        "{x=2461,m=1339,a=466,s=291}\n",
        "{x=2127,m=1623,a=2188,s=1013}\n",
    );

    #[test]
    fn should_parse_input() {
        let (_, _) = parsing::parse(INPUT).unwrap();
    }

    #[test]
    fn test_part1() {
        let (workflows, parts) = parsing::parse(INPUT).unwrap();
        assert_eq!(19114, part1(&workflows, &parts).unwrap());
    }

    #[test]
    fn test_part2() {
        let (workflows, _) = parsing::parse(INPUT).unwrap();
        assert_eq!(167409079868000, part2(&workflows).unwrap());
    }
}
