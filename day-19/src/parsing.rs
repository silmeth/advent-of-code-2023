use anyhow::anyhow;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, newline},
    combinator::{all_consuming, peek},
    error::Error as NomError,
    multi::many0,
    sequence::delimited,
    IResult,
};
use smallvec::SmallVec;
use std::collections::HashMap;

use crate::{
    Action, Condition, ConditionDescription, ConditionFn, Field, Part, Rule, Workflow, Workflows,
};

pub fn parse(input: &str) -> anyhow::Result<(Workflows<'_>, Vec<Part>)> {
    let (_, res) = all_consuming(parse_inner)(input).map_err(|e| anyhow!(format!("{e:?}")))?;
    Ok(res)
}

pub fn parse_inner<'a>(mut input: &'a str) -> IResult<&'a str, (Workflows<'a>, Vec<Part>)> {
    let mut workflows: HashMap<&'a str, Workflow<'a>> = HashMap::new();
    while peek(alpha1::<_, NomError<_>>)(input).is_ok() {
        let (name, workflow);
        (input, (name, workflow)) = parse_workflow(input)?;
        (input, _) = newline::<_, NomError<_>>(input)?;
        workflows.insert(name, workflow);
    }

    (input, _) = newline::<_, NomError<_>>(input)?;

    let parts;
    (input, parts) = many0(|input| {
        let (input, part) = parse_part(input)?;
        let (input, _) = newline(input)?;
        Ok((input, part))
    })(input)?;

    Ok((input, (Workflows { inner: workflows }, parts)))
}

fn parse_workflow(input: &str) -> IResult<&str, (&str, Workflow<'_>)> {
    let (input, name) = alpha1(input)?;

    let parse_rule = |input| {
        let (input, field_or_action) = alpha1(input)?;

        if let Ok((input, operator)) =
            alt::<_, _, NomError<_>, _>((tag("<"), tag(">"), tag("=")))(input)
        {
            let field = field_or_action;
            let (input, value) = nom::character::complete::u64(input)?;

            let (input, _) = tag(":")(input)?;
            let (input, action) = alpha1(input)?;

            let operator = match operator {
                "<" => std::cmp::Ordering::Less,
                ">" => std::cmp::Ordering::Greater,
                "=" => std::cmp::Ordering::Equal,
                _ => unreachable!(),
            };

            let action = Action::from_string(action);
            let field = match field {
                "x" => Field::CoolLook,
                "m" => Field::Musical,
                "a" => Field::Aerodynamic,
                "s" => Field::Shiny,
                _ => {
                    return Err(nom::Err::Error(NomError::new(
                        field,
                        nom::error::ErrorKind::Tag,
                    )))
                }
            };

            let condition_description = match operator {
                std::cmp::Ordering::Less => ConditionDescription {
                    accepted_range: 1..value,
                    field,
                },
                std::cmp::Ordering::Greater => ConditionDescription {
                    accepted_range: value + 1..4001,
                    field,
                },
                std::cmp::Ordering::Equal => ConditionDescription {
                    accepted_range: value..value + 1,
                    field,
                },
            };

            let condition_fn: ConditionFn = match field {
                Field::CoolLook => {
                    Box::new(move |part: &Part| part.cool_look.cmp(&value) == operator)
                }
                Field::Musical => Box::new(move |part: &Part| part.musical.cmp(&value) == operator),
                Field::Aerodynamic => {
                    Box::new(move |part: &Part| part.aerodynamic.cmp(&value) == operator)
                }
                Field::Shiny => Box::new(move |part: &Part| part.shiny.cmp(&value) == operator),
            };

            Ok((
                input,
                Rule {
                    condition: Condition {
                        description: Some(condition_description),
                        execute: condition_fn,
                    },
                    action,
                },
            ))
        } else {
            let action = Action::from_string(field_or_action);

            Ok((
                input,
                Rule {
                    condition: Condition {
                        execute: Box::new(|_| true),
                        description: None,
                    },
                    action,
                },
            ))
        }
    };

    let parse_rules = |mut input| {
        let mut rules = SmallVec::new();
        loop {
            let rule;
            (input, rule) = parse_rule(input)?;
            rules.push(rule);
            match tag::<_, _, NomError<_>>(",")(input) {
                Ok((rem, _)) => input = rem,
                Err(nom::Err::Error(_)) => break,
                Err(e) => return Err(e),
            }
        }

        Ok((input, rules))
    };

    let (input, rules) = delimited(tag("{"), parse_rules, tag("}"))(input)?;

    Ok((input, (name, rules)))
}

fn parse_part(input: &str) -> IResult<&str, Part> {
    let part_inner = |mut input| {
        let init_input = input;
        let mut cool_look = None;
        let mut musical = None;
        let mut aerodynamic = None;
        let mut shiny = None;

        loop {
            let (field, value);
            (input, field) = alpha1::<_, NomError<_>>(input)?;
            (input, _) = tag("=")(input)?;
            (input, value) = nom::character::complete::u64(input)?;

            match field {
                "x" => cool_look = Some(value),
                "m" => musical = Some(value),
                "a" => aerodynamic = Some(value),
                "s" => shiny = Some(value),
                _ => {
                    return Err(nom::Err::Error(NomError::new(
                        field,
                        nom::error::ErrorKind::Tag,
                    )))
                }
            };

            match tag::<_, _, NomError<_>>(",")(input) {
                Ok((rem, _)) => input = rem,
                Err(nom::Err::Error(_)) => break,
                Err(e) => return Err(e),
            }
        }

        let (Some(cool_look), Some(musical), Some(aerodynamic), Some(shiny)) =
            (cool_look, musical, aerodynamic, shiny)
        else {
            return Err(nom::Err::Failure(NomError::new(
                init_input,
                nom::error::ErrorKind::Tag,
            )));
        };

        Ok((
            input,
            Part {
                cool_look,
                musical,
                aerodynamic,
                shiny,
            },
        ))
    };

    delimited(tag("{"), part_inner, tag("}"))(input)
}
