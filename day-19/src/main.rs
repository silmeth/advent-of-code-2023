use day_19::{parsing::parse, part1, part2};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;
    let raw_input = &utils::read_entire_stdin()?;

    let (workflows, parts) = parse(raw_input)?;
    run(
        part,
        |(w, p)| part1(w, p),
        |(w, _)| part2(w),
        (&workflows, &parts),
    )
}
