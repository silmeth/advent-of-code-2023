use std::str::FromStr;

use anyhow::{anyhow, Context as _};
use utils::run;

struct Map {
    inner: Vec<Tile>,
    width: usize,
    height: usize,
    start: (usize, usize),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Dir {
    Up,
    Down,
    Left,
    Right,
}

impl Dir {
    fn invert(self) -> Self {
        match self {
            Dir::Up => Dir::Down,
            Dir::Down => Dir::Up,
            Dir::Left => Dir::Right,
            Dir::Right => Dir::Left,
        }
    }

    fn move_coords(self, x: usize, y: usize) -> (usize, usize) {
        match self {
            Dir::Up => (x, y - 1),
            Dir::Down => (x, y + 1),
            Dir::Left => (x - 1, y),
            Dir::Right => (x + 1, y),
        }
    }
}

impl Map {
    fn find_loop(&self) -> Option<Vec<(usize, usize, Tile)>> {
        let mut loop_elements = Vec::with_capacity(16);
        // push the start element – it makes a loop element too!
        loop_elements.push((
            self.start.0,
            self.start.1,
            self[(self.start.0, self.start.1)],
        ));
        let Some((mut tile, mut from_dir, (mut current_x, mut current_y))) = self.find_loop_start()
        else {
            return None;
        };

        let start_dir = from_dir.invert();

        loop {
            loop_elements.push((current_x, current_y, tile));

            let Some((next, dir, coords)) =
                self.find_next_neighbour(current_x, current_y, from_dir)
            else {
                return None;
            };
            (tile, from_dir, (current_x, current_y)) = (next, dir, coords);

            if tile == Tile::Start {
                break;
            }
        }

        let end_dir = {
            let (last_x, last_y, _) = loop_elements.last().unwrap();
            match (
                self.start.0 as isize - (*last_x as isize),
                self.start.1 as isize - (*last_y as isize),
            ) {
                (1, 0) => Dir::Left,
                (-1, 0) => Dir::Right,
                (0, 1) => Dir::Up,
                (0, -1) => Dir::Down,
                _ => unreachable!(),
            }
        };

        // change Start to its actual pipe shape
        let start_shape = match (end_dir, start_dir) {
            (Dir::Left, Dir::Right) | (Dir::Right, Dir::Left) => Tile::LeftRight,
            (Dir::Left, Dir::Up) | (Dir::Up, Dir::Left) => Tile::UpLeft,
            (Dir::Down, Dir::Up) | (Dir::Up, Dir::Down) => Tile::UpDown,
            (Dir::Right, Dir::Up) | (Dir::Up, Dir::Right) => Tile::UpRight,
            (Dir::Right, Dir::Down) | (Dir::Down, Dir::Right) => Tile::DownRight,
            (Dir::Left, Dir::Down) | (Dir::Down, Dir::Left) => Tile::DownLeft,
            _ => unreachable!(),
        };

        loop_elements[0].2 = start_shape;

        Some(loop_elements)
    }

    fn find_loop_start(&self) -> Option<(Tile, Dir, (usize, usize))> {
        let (x, y) = self.start;
        [Dir::Up, Dir::Right, Dir::Down, Dir::Left]
            .into_iter()
            .find_map(|dir| {
                self.towards(x, y, dir).and_then(|target| {
                    if target.connects_to(dir.invert()) {
                        Some((target, dir.invert(), dir.move_coords(x, y)))
                    } else {
                        None
                    }
                })
            })
    }

    fn find_next_neighbour(
        &self,
        x: usize,
        y: usize,
        from: Dir,
    ) -> Option<(Tile, Dir, (usize, usize))> {
        self[(x, y)]
            .possible_directions()
            .into_iter()
            .find_map(|dir| {
                self.towards(x, y, dir).and_then(|target| {
                    if from != dir && target.connects_to(dir.invert()) {
                        Some((target, dir.invert(), dir.move_coords(x, y)))
                    } else {
                        None
                    }
                })
            })
    }

    fn towards(&self, x: usize, y: usize, dir: Dir) -> Option<Tile> {
        match dir {
            Dir::Up => self.up_from(x, y),
            Dir::Down => self.down_from(x, y),
            Dir::Left => self.left_from(x, y),
            Dir::Right => self.right_from(x, y),
        }
    }

    fn up_from(&self, x: usize, y: usize) -> Option<Tile> {
        if y == 0 {
            None
        } else {
            Some(self[Dir::Up.move_coords(x, y)])
        }
    }
    fn down_from(&self, x: usize, y: usize) -> Option<Tile> {
        if y == self.height - 1 {
            None
        } else {
            Some(self[Dir::Down.move_coords(x, y)])
        }
    }
    fn left_from(&self, x: usize, y: usize) -> Option<Tile> {
        if x == 0 {
            None
        } else {
            Some(self[Dir::Left.move_coords(x, y)])
        }
    }
    fn right_from(&self, x: usize, y: usize) -> Option<Tile> {
        if x == self.width - 1 {
            None
        } else {
            Some(self[Dir::Right.move_coords(x, y)])
        }
    }
}

impl std::ops::Index<(usize, usize)> for Map {
    type Output = Tile;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        assert!(y < self.height);
        assert!(x < self.width);
        &self.inner[self.width * y + x]
    }
}

impl FromStr for Map {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (width, height) = {
            let mut lines = s.lines();
            let width = lines.next().context("expected multiline input")?.len();
            if lines.any(|line| line.len() != width) {
                return Err(anyhow!("map must be a perfect rectangle"));
            }
            // `width + 1` to account for `\n` in a line
            let height = s.len() / (width + 1);
            (width, height)
        };
        let (start, inner) = s
            .lines()
            .flat_map(str::chars)
            .map(Tile::try_from)
            .enumerate()
            .map(|(idx, tile)| {
                let x = idx % width;
                let y = idx / width;
                ((x, y), tile)
            })
            .try_fold(
                (None, Vec::with_capacity(256)),
                |(start_coord, mut inner), next| {
                    let (new_coord, tile) = next;
                    let tile = tile?;
                    inner.push(tile);

                    if tile == Tile::Start {
                        Ok::<_, anyhow::Error>((Some(new_coord), inner))
                    } else {
                        Ok((start_coord, inner))
                    }
                },
            )?;

        let start = start.context("Start tile not found")?;

        Ok(Self {
            inner,
            width,
            height,
            start,
        })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Tile {
    UpDown,
    LeftRight,
    UpRight,
    UpLeft,
    DownLeft,
    DownRight,
    Ground,
    Start,
}

impl Tile {
    fn connects_to(&self, dir: Dir) -> bool {
        matches!(
            (dir, self),
            (_, Tile::Start)
                | (Dir::Down, Tile::UpDown | Tile::DownLeft | Tile::DownRight)
                | (Dir::Up, Tile::UpDown | Tile::UpLeft | Tile::UpRight)
                | (
                    Dir::Right,
                    Tile::LeftRight | Tile::UpRight | Tile::DownRight
                )
                | (Dir::Left, Tile::LeftRight | Tile::UpLeft | Tile::DownLeft)
        )
    }

    fn possible_directions(self) -> [Dir; 2] {
        match self {
            Tile::UpDown => [Dir::Up, Dir::Down],
            Tile::LeftRight => [Dir::Left, Dir::Right],
            Tile::UpRight => [Dir::Up, Dir::Right],
            Tile::UpLeft => [Dir::Up, Dir::Left],
            Tile::DownLeft => [Dir::Down, Dir::Left],
            Tile::DownRight => [Dir::Down, Dir::Right],
            Tile::Ground | Tile::Start => panic!("cannot go through S or ground"),
        }
    }
}

impl TryFrom<char> for Tile {
    type Error = anyhow::Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '|' => Ok(Tile::UpDown),
            '-' => Ok(Tile::LeftRight),
            'L' => Ok(Tile::UpRight),
            'J' => Ok(Tile::UpLeft),
            '7' => Ok(Tile::DownLeft),
            'F' => Ok(Tile::DownRight),
            '.' => Ok(Tile::Ground),
            'S' => Ok(Tile::Start),
            _ => Err(anyhow!("unknown tile type `{}`", value)),
        }
    }
}

fn part1(map: &Map) -> anyhow::Result<u32> {
    let loopy_loop = map.find_loop().context("loop not found")?;

    Ok((u32::try_from(loopy_loop.len()).context("loop too large")?) / 2)
}

fn part2(map: &Map) -> anyhow::Result<u32> {
    let loopy_loop = map.find_loop().context("loop not found")?;

    // let’s integrate over regions between horizontal lines in vertical slices labeled `x`
    let mut area = 0;
    for x in 0..map.width {
        // consider only horizontal (non-|) tiles as upper and lower boundaries
        let mut bounds: Vec<_> = loopy_loop
            .iter()
            .filter(|(tile_x, _, tile)| *tile_x == x && *tile != Tile::UpDown)
            .collect();
        bounds.sort_by_key(|(_, y, _)| y);

        let mut i = 0;
        'outer: while i < bounds.len() {
            // calculate first edge
            let first = loop {
                let current = bounds[i];
                i += 1;
                // filter out C-shape vertical lines; get lower part of S-shaped vertical parts
                if [Tile::DownRight, Tile::DownLeft].contains(&current.2) {
                    let next = bounds[i];
                    i += 1;
                    match (current.2, next.2) {
                        // S-shape
                        (Tile::DownLeft, Tile::UpRight) | (Tile::DownRight, Tile::UpLeft) => {
                            break next.1
                        }
                        // C-shape
                        _ => {
                            if i >= bounds.len() {
                                break 'outer;
                            }
                        }
                    };
                } else {
                    break current.1;
                }
            };

            let second = {
                let current = bounds[i];
                i += 1;
                // take first part of S-shaped vertical walls, but skip their lower part; don’t skip
                // C-shapes
                if [Tile::DownRight, Tile::DownLeft].contains(&current.2) {
                    let next = bounds[i];
                    i += 1;
                    match (current.2, next.2) {
                        // S-shape
                        (Tile::DownLeft, Tile::UpRight) | (Tile::DownRight, Tile::UpLeft) => {
                            current.1
                        }
                        // C-shape
                        _ => {
                            i -= 1;
                            current.1
                        }
                    }
                } else {
                    current.1
                }
            };

            let area_increment = second - first - 1;
            area += u32::try_from(area_increment).context("area out of bounds")?;
        }
    }

    Ok(area)
}

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let map: Map = utils::read_entire_stdin()?.parse()?;

    run(part, part1, part2, &map)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT1: &str = concat!("..F7.\n", ".FJ|.\n", "SJ.L7\n", "|F--J\n", "LJ...\n",);
    const INPUT2_1: &str = concat!(
        "...........\n",
        ".S-------7.\n",
        ".|F-----7|.\n",
        ".||.....||.\n",
        ".||.....||.\n",
        ".|L-7.F-J|.\n",
        ".|..|.|..|.\n",
        ".L--J.L--J.\n",
        "...........\n",
    );
    const INPUT2_2: &str = concat!(
        ".F----7F7F7F7F-7....\n",
        ".|F--7||||||||FJ....\n",
        ".||.FJ||||||||L7....\n",
        "FJL7L7LJLJ||LJ.L-7..\n",
        "L--J.L7...LJS7F-7L7.\n",
        "....F-J..F7FJ|L7L7L7\n",
        "....L7.F7||L7|.L7L7|\n",
        ".....|FJLJ|FJ|F7|.LJ\n",
        "....FJL-7.||.||||...\n",
        "....L---J.LJ.LJLJ...\n",
    );
    const INPUT2_3: &str = concat!(
        "FF7FSF7F7F7F7F7F---7\n",
        "L|LJ||||||||||||F--J\n",
        "FL-7LJLJ||||||LJL-77\n",
        "F--JF--7||LJLJ7F7FJ-\n",
        "L---JF-JLJ.||-FJLJJ7\n",
        "|F|F-JF---7F7-L7L|7|\n",
        "|FFJF7L7F-JF7|JL---7\n",
        "7-L-JL7||F7|L7F-7F7|\n",
        "L.L7LFJ|||||FJL7||LJ\n",
        "L7JLJL-JLJLJL--JLJ.L\n",
    );

    #[test]
    fn example_input_part1() {
        let result = part1(&INPUT1.parse().unwrap()).unwrap();
        assert_eq!(8, result);
    }

    #[test]
    fn example_input1_part2() {
        let result = part2(&INPUT2_1.parse().unwrap()).unwrap();
        assert_eq!(4, result);
    }

    #[test]
    fn example_input2_part2() {
        let result = part2(&INPUT2_2.parse().unwrap()).unwrap();
        assert_eq!(8, result);
    }

    #[test]
    fn example_input3_part2() {
        let result = part2(&INPUT2_3.parse().unwrap()).unwrap();
        assert_eq!(10, result);
    }
}
