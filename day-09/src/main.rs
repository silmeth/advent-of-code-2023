use std::io::BufRead as _;

use anyhow::Context as _;
use utils::run;

fn part1(measurements: &[Vec<i64>]) -> i64 {
    measurements.iter().map(|m| extrapolate_next(m)).sum()
}

fn part2(measurements: &[Vec<i64>]) -> i64 {
    measurements.iter().map(|m| extrapolate_prev(m)).sum()
}

fn extrapolate_next(measurements: &[i64]) -> i64 {
    let diff_series = gen_diff_series(measurements);
    diff_series.iter().map(|it| it.last().unwrap()).sum()
}

fn extrapolate_prev(measurements: &[i64]) -> i64 {
    let diff_series = gen_diff_series(measurements);

    let mut lower_initial = 0;
    for initial in diff_series.iter().rev().map(|it| it[0]) {
        lower_initial = initial - lower_initial;
    }
    lower_initial
}

fn gen_diff_series(measurements: &[i64]) -> Vec<Vec<i64>> {
    let mut diff_series = vec![measurements.to_owned()];

    while diff_series.last().unwrap().iter().any(|n| *n != 0) {
        let new_diff = differences(diff_series.last().unwrap());
        diff_series.push(new_diff);
    }

    diff_series
}

fn differences(input: &[i64]) -> Vec<i64> {
    input
        .windows(2)
        .map(|window| window[1] - window[0])
        .collect()
}

fn read_measurements(line: &str) -> anyhow::Result<Vec<i64>> {
    line.split_whitespace()
        .map(|s| s.parse().context("expected numbers"))
        .collect()
}

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let stdin = std::io::stdin().lock();
    let measurements: Vec<_> = stdin
        .lines()
        .map(|line| {
            let line = line.context("failed to read input line")?;
            read_measurements(&line)
        })
        .collect::<anyhow::Result<_>>()?;

    run(part, |m| Ok(part1(m)), |m| Ok(part2(m)), &measurements[..])
}

#[cfg(test)]
mod tests {
    use super::*;
    const INPUT: &str = concat!("0 3 6 9 12 15\n", "1 3 6 10 15 21\n", "10 13 16 21 30 45\n",);

    #[test]
    fn test_part1_sample() {
        let measurements: Vec<_> = INPUT
            .lines()
            .map(read_measurements)
            .collect::<anyhow::Result<_>>()
            .unwrap();
        let res = part1(&measurements);
        assert_eq!(114, res);
    }

    #[test]
    fn test_part2_sample() {
        let measurements: Vec<_> = INPUT
            .lines()
            .map(read_measurements)
            .collect::<anyhow::Result<_>>()
            .unwrap();
        let res = part2(&measurements);
        assert_eq!(2, res);
    }
}
