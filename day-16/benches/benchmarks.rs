use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_16::*;

static INPUT: &str = include_str!("input");

fn parse_benchmark(c: &mut Criterion) {
    c.bench_function("day 16, parsing", |b| {
        b.iter(|| black_box(INPUT).parse::<Contraption>().unwrap())
    });
}

fn part1_benchmark(c: &mut Criterion) {
    let mut contraption = INPUT.parse::<Contraption>().unwrap();
    c.bench_function("day 16, part 1", |b| {
        b.iter(|| assert_eq!(7798, part1(black_box(&mut contraption))))
    });
}

fn part2_benchmark(c: &mut Criterion) {
    let mut contraption = INPUT.parse::<Contraption>().unwrap();
    c.bench_function("day 16, part 2", |b| {
        b.iter(|| assert_eq!(8026, part2(black_box(&mut contraption))))
    });
}

criterion_group!(benches, parse_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);
