use crossterm::{cursor, terminal, QueueableCommand as _};
use day_16::{Beam, Contraption, Direction};
use std::io::Write;
use std::time::{Duration, Instant};
use utils::read_entire_stdin;

fn main() -> anyhow::Result<()> {
    let input = read_entire_stdin()?;
    let mut contraption: Contraption = input.parse()?;
    let mut stdout = std::io::stdout().lock();

    let mut prev = Instant::now();
    stdout.queue(cursor::Hide)?;
    stdout.queue(cursor::MoveTo(0, 0))?;
    stdout.queue(terminal::Clear(terminal::ClearType::All))?;
    write_contraption(&mut stdout, &contraption);
    stdout.queue(cursor::MoveTo(0, 0))?;
    stdout.flush()?;

    contraption.cast_and_monitor(Beam::new(0, 0, Direction::E), |contraption| {
        let now = Instant::now();
        std::thread::sleep((prev + Duration::from_millis(125)) - now);
        prev = now;
        stdout.queue(cursor::MoveTo(0, 0)).unwrap();
        write_contraption(&mut stdout, contraption);
        stdout.queue(cursor::MoveTo(0, 0)).unwrap();
        stdout.flush().unwrap();
    });

    std::thread::sleep(Duration::from_millis(500));
    Ok(())
}

fn write_contraption<W: Write>(mut writer: W, contraption: &Contraption) {
    const MAX_LINES: usize = 64;
    format!("{contraption}")
        .lines()
        .take(MAX_LINES)
        .for_each(|line| writeln!(writer, "{line}").unwrap())
}
