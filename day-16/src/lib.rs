use anyhow::{anyhow, Context as _};
use flagset::{flags, FlagSet};

pub struct Contraption {
    tiles: Vec<Tile>,
    width: usize,
    height: usize,
}

impl Contraption {
    #[inline]
    pub fn cast_beam(&mut self, beam: Beam) {
        self.cast_and_monitor(
            beam,
            #[inline]
            |_| {},
        )
    }

    pub fn cast_and_monitor<F: FnMut(&Self)>(&mut self, beam: Beam, mut on_update: F) {
        let mut active_beams: Vec<Beam> = vec![beam];
        let mut next_beams = vec![];

        while !active_beams.is_empty() {
            for beam in &active_beams {
                let tile = &mut self[(beam.x, beam.y)];
                let typ = tile.typ;
                if tile.beams_to_dir.contains(beam.dir) {
                    // beam already processed
                    continue;
                }

                // mark as processed
                tile.beams_to_dir |= beam.dir;

                self.spawn_new_beams(beam, typ, &mut next_beams);
            }

            on_update(self);
            active_beams.clear();
            std::mem::swap(&mut active_beams, &mut next_beams);
        }
    }

    pub fn reset(&mut self) {
        for tile in &mut self.tiles {
            tile.beams_to_dir = FlagSet::default();
        }
    }

    pub fn count_energized(&self) -> usize {
        self.tiles
            .iter()
            .filter(|t| !t.beams_to_dir.is_empty())
            .count()
    }

    fn spawn_new_beams(&self, beam: &Beam, tile: TileType, next_beams: &mut Vec<Beam>) {
        match tile {
            TileType::Splitter(dirs) if dirs.contains(beam.dir) => next_beams.extend(
                beam.split()
                    .into_iter()
                    .filter(|b| self.pos_in_bounds(b.x, b.y)),
            ),
            TileType::Ground | TileType::Splitter(_) => {
                let new_beam = beam.move_step();
                if self.pos_in_bounds(new_beam.x, new_beam.y) {
                    next_beams.push(new_beam);
                }
            }
            TileType::Mirror(diag) => {
                let new_beam = beam.reflect(diag);
                if self.pos_in_bounds(new_beam.x, new_beam.y) {
                    next_beams.push(new_beam);
                }
            }
        }
    }

    #[inline]
    fn pos_in_bounds(&self, x: usize, y: usize) -> bool {
        x < self.width && y < self.height
    }
}

impl std::ops::Index<(usize, usize)> for Contraption {
    type Output = Tile;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        assert!(self.pos_in_bounds(x, y));

        let idx = y * self.width + x;
        &self.tiles[idx]
    }
}

impl std::ops::IndexMut<(usize, usize)> for Contraption {
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut Self::Output {
        assert!(x < self.width);
        assert!(y < self.height);

        let idx = y * self.width + x;
        &mut self.tiles[idx]
    }
}

impl std::str::FromStr for Contraption {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let width: usize = s
            .lines()
            .next()
            .context("expected at least one line")?
            .len();
        let height: usize = s.lines().count();

        let tiles: Vec<Tile> = s
            .lines()
            .flat_map(|line| line.bytes().enumerate())
            .map(|(x, c)| {
                if x >= width {
                    return Err(anyhow!("all lines should be of equal length"));
                }
                Ok(Tile::new(c.try_into()?))
            })
            .collect::<anyhow::Result<_>>()?;

        Ok(Contraption {
            tiles,
            width,
            height,
        })
    }
}

pub struct Tile {
    typ: TileType,
    beams_to_dir: FlagSet<Direction>,
}

impl Tile {
    fn new(typ: TileType) -> Self {
        Self {
            typ,
            beams_to_dir: FlagSet::default(),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Beam {
    x: usize,
    y: usize,
    dir: Direction,
}

impl Beam {
    pub fn new(x: usize, y: usize, dir: Direction) -> Self {
        Self { x, y, dir }
    }

    fn move_step(&self) -> Self {
        match self.dir {
            Direction::N => Self {
                y: self.y.wrapping_sub(1),
                ..*self
            },
            Direction::E => Self {
                x: self.x.wrapping_add(1),
                ..*self
            },
            Direction::S => Self {
                y: self.y.wrapping_add(1),
                ..*self
            },
            Direction::W => Self {
                x: self.x.wrapping_sub(1),
                ..*self
            },
        }
    }

    fn split(&self) -> [Self; 2] {
        match self.dir {
            Direction::N | Direction::S => [
                Self {
                    dir: Direction::E,
                    ..*self
                }
                .move_step(),
                Self {
                    dir: Direction::W,
                    ..*self
                }
                .move_step(),
            ],
            Direction::W | Direction::E => [
                Self {
                    dir: Direction::N,
                    ..*self
                }
                .move_step(),
                Self {
                    dir: Direction::S,
                    ..*self
                }
                .move_step(),
            ],
        }
    }

    fn reflect(&self, diagonal: Diagonal) -> Self {
        match (self.dir, diagonal) {
            (Direction::N, Diagonal::LeftToUp) | (Direction::S, Diagonal::LeftToDown) => Self {
                dir: Direction::E,
                ..*self
            }
            .move_step(),
            (Direction::N, Diagonal::LeftToDown) | (Direction::S, Diagonal::LeftToUp) => Self {
                dir: Direction::W,
                ..*self
            }
            .move_step(),
            (Direction::E, Diagonal::LeftToDown) | (Direction::W, Diagonal::LeftToUp) => Self {
                dir: Direction::S,
                ..*self
            }
            .move_step(),
            (Direction::E, Diagonal::LeftToUp) | (Direction::W, Diagonal::LeftToDown) => Self {
                dir: Direction::N,
                ..*self
            }
            .move_step(),
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum TileType {
    Ground,
    /// splits beams coming in the directions listed in the flag set
    Splitter(FlagSet<Direction>),
    /// reflects beams according to its set-up
    Mirror(Diagonal),
}

impl TryFrom<u8> for TileType {
    type Error = anyhow::Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            b'.' => Ok(TileType::Ground),
            b'|' => Ok(TileType::Splitter(Direction::W | Direction::E)),
            b'-' => Ok(TileType::Splitter(Direction::N | Direction::S)),
            b'/' => Ok(TileType::Mirror(Diagonal::LeftToUp)),
            b'\\' => Ok(TileType::Mirror(Diagonal::LeftToDown)),
            _ => Err(anyhow!(
                "Unknown tile type {}",
                char::try_from(value as u32).with_context(|| format!("non-ASCII byte {value}"))?
            )),
        }
    }
}

flags! {
    pub enum Direction: u8 {
        N,
        E,
        S,
        W,
    }
}

#[derive(Debug, Clone, Copy)]
enum Diagonal {
    LeftToUp,
    LeftToDown,
}

pub fn part1(contraption: &mut Contraption) -> usize {
    contraption.cast_beam(Beam::new(0, 0, Direction::E));
    contraption.count_energized()
}

pub fn part2(contraption: &mut Contraption) -> usize {
    let max_y = contraption.height - 1;
    let max_x = contraption.width - 1;
    // column: x = 0
    (0..contraption.height)
        .map(|y| Beam::new(0, y, Direction::E))
        // column: x = contraption.width - 1
        .chain((0..contraption.height).map(|y| Beam::new(max_x, y, Direction::W)))
        // row: y = 0
        .chain((0..contraption.width).map(|x| Beam::new(x, 0, Direction::S)))
        // row: y = contraption.height - 1
        .chain((0..contraption.width).map(|x| Beam::new(x, max_y, Direction::N)))
        .map(|beam| {
            contraption.cast_beam(beam);
            let res = contraption.count_energized();
            contraption.reset();
            res
        })
        .max()
        .expect("contraption is never empty")
}

impl std::fmt::Display for Contraption {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                let tile = &self[(x, y)];
                if tile.beams_to_dir.is_empty() {
                    match tile.typ {
                        TileType::Ground => write!(f, ".")?,
                        TileType::Splitter(dirs) if dirs.contains(Direction::W) => write!(f, "|")?,
                        TileType::Splitter(_) => write!(f, "-")?,
                        TileType::Mirror(Diagonal::LeftToUp) => write!(f, "/")?,
                        TileType::Mirror(_) => write!(f, "\\")?,
                    }
                } else {
                    write!(f, "#")?;
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        ".|...\\....\n",
        "|.-.\\.....\n",
        ".....|-...\n",
        "........|.\n",
        "..........\n",
        ".........\\\n",
        "..../.\\\\..\n",
        ".-.-/..|..\n",
        ".|....-|.\\\n",
        "..//.|....\n",
    );

    #[test]
    fn test_part1() {
        let mut contraption: Contraption = INPUT.parse().unwrap();
        let result = part1(&mut contraption);
        assert_eq!(46, result);
    }

    #[test]
    fn test_part2() {
        let mut contraption: Contraption = INPUT.parse().unwrap();
        let result = part2(&mut contraption);
        assert_eq!(51, result);
    }
}
