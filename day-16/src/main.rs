use day_16::{part1, part2, Contraption};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let mut contraption: Contraption = utils::read_entire_stdin()?.parse()?;

    run(
        part,
        |contraption| Ok(part1(contraption)),
        |contraption| Ok(part2(contraption)),
        &mut contraption,
    )
}
