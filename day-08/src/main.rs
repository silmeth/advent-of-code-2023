use anyhow::{anyhow, Context as _};
use num::integer::lcm;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::str::FromStr;
use utils::expect;
use utils::run;

struct Map {
    steps: Vec<Step>,
    map: HashMap<Plot, (Plot, Plot)>,
}

impl Map {
    fn distance<End>(&self, start: Plot, mut end: End) -> anyhow::Result<u64>
    where
        End: FnMut(Plot) -> bool,
    {
        let mut num = 0;
        let mut current = start;
        for step in std::iter::repeat(self.steps.iter()).flatten() {
            if end(current) {
                break;
            }

            let (left, right) = self.map.get(&current).context("unknown plot")?;
            current = match step {
                Step::Left => *left,
                Step::Right => *right,
            };
            num += 1;
        }

        Ok(num)
    }
}

impl FromStr for Map {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> anyhow::Result<Self> {
        let (steps, map) = s.split_once('\n').context("expected multi-line input")?;
        let map = map.trim();

        let steps = steps
            .chars()
            .map(Step::try_from)
            .collect::<anyhow::Result<_>>()?;

        let plots = map.lines().map(|line| {
            let (key, remaining) = line
                .split_once(" = ")
                .context("expected {plot} = ({plot}, {plot})")?;
            let key: Plot = key.parse()?;
            let remaining = expect("(", remaining)?;
            let (left, remaining) = remaining
                .split_once(',')
                .context("expected {plot}, {plot}")?;
            let left = left.parse()?;
            let remaining = remaining.trim();
            let (right, remaining) = remaining.split_once(')').context("expected {plot})")?;
            let right = right.parse()?;
            if !remaining.is_empty() {
                return Err(anyhow!("expected end of input after map plots"));
            }

            Ok((key, (left, right)))
        });

        let map: HashMap<_, _> = plots.collect::<anyhow::Result<_>>()?;

        Ok(Self { steps, map })
    }
}

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy)]
struct Plot([u8; 3]);

impl FromStr for Plot {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> anyhow::Result<Self> {
        if s.len() != 3 || s.chars().any(|c| !c.is_ascii_uppercase()) {
            return Err(anyhow!(
                "plot must be 3 upper-case ASCII letters (got {})",
                s
            ));
        }

        Ok(Self(
            s.as_bytes()
                .try_into()
                .expect("`s` is an ASCII string of length 3"),
        ))
    }
}

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy)]
enum Step {
    Left,
    Right,
}

impl TryFrom<char> for Step {
    type Error = anyhow::Error;

    fn try_from(c: char) -> anyhow::Result<Self> {
        match c {
            'L' => Ok(Self::Left),
            'R' => Ok(Self::Right),
            _ => Err(anyhow!("unrecognized step instruction")),
        }
    }
}

fn part1(map: &Map) -> anyhow::Result<u64> {
    map.distance(Plot(*b"AAA"), |plot| &plot.0 == b"ZZZ")
}

fn part2(map: &Map) -> anyhow::Result<u64> {
    // Let’s assume every plot in A starts a repeating cycle ending in a plot in Z which leads to the
    // same plot as A did initially.
    //
    // It’s *NOT* a generic solution but seems to be working for all puzzle inputs.
    let res = map
        .map
        .keys()
        .cloned()
        .filter(|plot| plot.0[2] == b'A')
        .map(|plot| map.distance(plot, |p| p.0[2] == b'Z'))
        .try_fold(1, |acc, res| res.map(|dist| lcm(acc, dist)))?;

    Ok(res)
}

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let map: Map = utils::read_entire_stdin()?.parse()?;

    run(part, part1, part2, &map)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT1: &str = concat!(
        "RL\n",
        "\n",
        "AAA = (BBB, CCC)\n",
        "BBB = (DDD, EEE)\n",
        "CCC = (ZZZ, GGG)\n",
        "DDD = (DDD, DDD)\n",
        "EEE = (EEE, EEE)\n",
        "GGG = (GGG, GGG)\n",
        "ZZZ = (ZZZ, ZZZ)\n",
    );

    const INPUT2: &str = concat!(
        "LLR\n",
        "\n",
        "AAA = (BBB, BBB)\n",
        "BBB = (AAA, ZZZ)\n",
        "ZZZ = (ZZZ, ZZZ)\n",
    );

    // AoC example uses `1` and `2` as “filler” characters, here changed to `A` and `B`
    // since we accept only ASCII uppercase
    const INPUT_PART2: &str = concat!(
        "LR\n",
        "\n",
        "AAA = (AAB, XXX)\n",
        "AAB = (XXX, AAZ)\n",
        "AAZ = (AAB, XXX)\n",
        "BBA = (BBB, XXX)\n",
        "BBB = (BBC, BBC)\n",
        "BBC = (BBZ, BBZ)\n",
        "BBZ = (BBB, BBB)\n",
        "XXX = (XXX, XXX)\n",
    );

    #[test]
    fn example_input1_part1() {
        let result = part1(&INPUT1.parse().unwrap()).unwrap();
        assert_eq!(2, result);
    }

    #[test]
    fn example_input2_part1() {
        let result = part1(&INPUT2.parse().unwrap()).unwrap();
        assert_eq!(6, result);
    }

    #[test]
    fn example_input_part2() {
        let result = part2(&INPUT_PART2.parse().unwrap()).unwrap();
        assert_eq!(6, result);
    }
}
