use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_14::*;
use std::time::{Duration, Instant};

static INPUT: &str = include_str!("input");

fn parsing_benchmark(c: &mut Criterion) {
    c.bench_function("day 14, parsing", |b| {
        b.iter(|| black_box(INPUT).parse::<Rocks>().unwrap())
    });
}

fn perform_benchmark<R: for<'a> FnMut(&'a mut Rocks)>(
    name: &'static str,
    mut routine: R,
    c: &mut Criterion,
) {
    let input = INPUT.parse::<Rocks>().unwrap();
    c.bench_function(name, |b| {
        b.iter_custom(|iters| {
            let mut time = Duration::ZERO;
            for _ in 0..iters {
                let mut input = input.clone();
                let start = Instant::now();
                routine(&mut input);
                time += start.elapsed();
            }
            time
        })
    });
}

fn part1_benchmark(c: &mut Criterion) {
    perform_benchmark(
        "day 14, part 1",
        |input| assert_eq!(109596, part1(black_box(input)).unwrap()),
        c,
    )
}

fn part2_benchmark(c: &mut Criterion) {
    perform_benchmark(
        "day 14, part 2",
        |input| assert_eq!(96105, part2(black_box(input)).unwrap()),
        c,
    )
}

criterion_group!(benches, parsing_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);
