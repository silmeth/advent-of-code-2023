use day_14::{part1, part2};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let input = utils::read_entire_stdin()?;
    let mut rocks = input.parse()?;

    run(part, part1, part2, &mut rocks)
}
