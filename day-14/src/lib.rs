use anyhow::{anyhow, Context as _};
use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::{cmp::Reverse, str::FromStr};

#[derive(Clone)]
pub struct Rocks {
    /// vector of positions of every rock in the input
    rocks: Vec<(u8, u8, RockType)>,
    height: u8,
    width: u8,
}

impl std::fmt::Display for Rocks {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                match self.rocks.iter().find(|(xr, yr, _)| *xr == x && *yr == y) {
                    Some((_, _, RockType::Cube)) => f.write_str("#")?,
                    Some((_, _, RockType::Round)) => f.write_str("O")?,
                    None => f.write_str(".")?,
                }
            }
            f.write_str("\n")?;
        }
        Ok(())
    }
}

pub trait Direction {
    type O;
    /// rotate the values so that stones “fall” towards small y values
    fn rot(dimensions: (u8, u8), x: u8, y: u8) -> (u8, u8);
    /// choose the mutable axis which points vertically under rotation
    fn rot_mut_y<'a>(x: &'a mut u8, y: &'a mut u8) -> &'a mut u8;
    fn sort_key(x: u8, y: u8) -> (u8, Self::O);

    fn search_needle(dimensions: (u8, u8), x: u8) -> (u8, Self::O) {
        let y = Self::rot_y(dimensions, 0, 0);
        (Self::sort_key(x, x).0, Self::sort_key(y, y).1)
    }
    fn rot_x(x: u8, y: u8) -> u8 {
        Self::rot((u8::MAX, u8::MAX), x, y).0
    }
    fn rot_y(dimensions: (u8, u8), x: u8, y: u8) -> u8 {
        Self::rot(dimensions, x, y).1
    }
}

pub struct North;
impl Direction for North {
    type O = u8;
    fn rot(_: (u8, u8), x: u8, y: u8) -> (u8, u8) {
        (x, y)
    }
    fn rot_mut_y<'a>(_: &'a mut u8, y: &'a mut u8) -> &'a mut u8 {
        y
    }
    fn sort_key(x: u8, y: u8) -> (u8, Self::O) {
        (x, y)
    }
}
pub struct West;
impl Direction for West {
    type O = u8;
    fn rot(_: (u8, u8), x: u8, y: u8) -> (u8, u8) {
        (y, x)
    }
    fn rot_mut_y<'a>(x: &'a mut u8, _: &'a mut u8) -> &'a mut u8 {
        x
    }
    fn sort_key(x: u8, y: u8) -> (u8, Self::O) {
        (y, x)
    }
}
pub struct South;
impl Direction for South {
    type O = Reverse<u8>;
    fn rot((_, height): (u8, u8), x: u8, y: u8) -> (u8, u8) {
        (x, height - y - 1)
    }
    fn rot_mut_y<'a>(_: &'a mut u8, y: &'a mut u8) -> &'a mut u8 {
        y
    }
    fn sort_key(x: u8, y: u8) -> (u8, Self::O) {
        (x, Reverse(y))
    }
}
pub struct East;
impl Direction for East {
    type O = Reverse<u8>;
    fn rot((width, _): (u8, u8), x: u8, y: u8) -> (u8, u8) {
        (y, width - x - 1)
    }
    fn rot_mut_y<'a>(x: &'a mut u8, _: &'a mut u8) -> &'a mut u8 {
        x
    }
    fn sort_key(x: u8, y: u8) -> (u8, Self::O) {
        (y, Reverse(x))
    }
}

impl Rocks {
    pub fn north_load(&self) -> usize {
        self.rocks
            .iter()
            .map(|(_, y, rock)| {
                if *rock == RockType::Round {
                    usize::from(self.height - *y)
                } else {
                    0
                }
            })
            .sum()
    }

    pub fn tilt<D: Direction>(&mut self)
    where
        <D as Direction>::O: Ord,
    {
        let dimensions = (self.width, self.height);
        self.rocks.sort_by_key(|(x, y, _)| D::sort_key(*x, *y));

        let mut rocks_remaining = &mut self.rocks[..];
        let cols = D::rot_x(self.width, self.height);
        for x in 0..cols {
            let idx = match rocks_remaining.binary_search_by_key(
                &D::search_needle(dimensions, x + 1),
                |(x_rock, y_rock, _)| D::sort_key(*x_rock, *y_rock),
            ) {
                Ok(idx) | Err(idx) => idx,
            };

            let (x_rocks, next) = rocks_remaining.split_at_mut(idx);
            rocks_remaining = next;

            let mut uppermost = 0;

            for (x_cur, y_cur, rock) in x_rocks.iter_mut() {
                if *rock == RockType::Round {
                    *D::rot_mut_y(x_cur, y_cur) = D::rot_y(dimensions, uppermost, uppermost);
                    uppermost += 1;
                } else {
                    uppermost = D::rot_y(dimensions, *x_cur, *y_cur) + 1;
                }
            }
        }
    }
}

impl FromStr for Rocks {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let width: u8 = s
            .lines()
            .next()
            .context("expected at least one line")?
            .len()
            .try_into()
            .context("width out of bounds")?;
        let height: u8 = s
            .lines()
            .count()
            .try_into()
            .context("height out of bounds")?;

        let rocks = s
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.bytes()
                    .enumerate()
                    .filter(|(_, c)| *c != b'.')
                    .map(move |(x, c)| (x, y, c))
            })
            .map(|(x, y, c)| {
                if x >= width.into() {
                    return Err(anyhow!("all lines should be of equal length"));
                }
                c.try_into().and_then(|rock| {
                    Ok((
                        x.try_into().context("x out of bounds")?,
                        y.try_into().context("y out of bounds")?,
                        rock,
                    ))
                })
            })
            .collect::<anyhow::Result<_>>()?;
        Ok(Self {
            rocks,
            width,
            height,
        })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum RockType {
    Round,
    Cube,
}

impl TryFrom<u8> for RockType {
    type Error = anyhow::Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            b'O' => Ok(Self::Round),
            b'#' => Ok(Self::Cube),
            _ => Err(anyhow!("unknown rock type: {}", char::from(value))),
        }
    }
}

pub fn part1(rocks: &mut Rocks) -> anyhow::Result<usize> {
    rocks.tilt::<North>();
    Ok(rocks.north_load())
}

pub fn part2(rocks: &mut Rocks) -> anyhow::Result<usize> {
    let mut seen_configurations = HashMap::with_capacity(128);
    let max_iterations = 1_000_000_000;
    let mut remaining_iterations = max_iterations;
    let mut cycle_detected = false;

    while remaining_iterations > 0 {
        rocks.tilt::<North>();
        rocks.tilt::<West>();
        rocks.tilt::<South>();
        rocks.tilt::<East>();

        let n = max_iterations - remaining_iterations;
        remaining_iterations -= 1;

        if !cycle_detected {
            let mut hasher = DefaultHasher::new();
            rocks
                .rocks
                .iter()
                .for_each(|(x, y, _)| (x, y).hash(&mut hasher));
            let hash = hasher.finish();

            if let Some(prev_iteration) = seen_configurations.get(&hash) {
                let cycle_length = n - prev_iteration;
                remaining_iterations = (max_iterations - 1 - prev_iteration) % cycle_length;
                cycle_detected = true;
            } else {
                seen_configurations.insert(hash, n);
            }
        }
    }
    Ok(rocks.north_load())
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "O....#....\n",
        "O.OO#....#\n",
        ".....##...\n",
        "OO.#O....O\n",
        ".O.....O#.\n",
        "O.#..O.#.#\n",
        "..O..#O..O\n",
        ".......O..\n",
        "#....###..\n",
        "#OO..#....\n",
    );

    #[test]
    fn test_part1() {
        let mut rocks = INPUT.parse().unwrap();
        let result = part1(&mut rocks).unwrap();
        assert_eq!(136, result);
    }

    #[test]
    fn test_part2() {
        let mut rocks = INPUT.parse().unwrap();
        let result = part2(&mut rocks).unwrap();
        assert_eq!(64, result);
    }
}
