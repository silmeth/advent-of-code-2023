use anyhow::{anyhow, Context};
use std::io::BufRead;
use utils::{expect, expected_err, run};

fn part1<I: Iterator<Item = anyhow::Result<Game>>>(games: I) -> anyhow::Result<usize> {
    games
        .filter(|g| match &g {
            Ok(g) => g
                .subsets
                .iter()
                .all(|set| set.red <= 12 && set.green <= 13 && set.blue <= 14),
            Err(_) => true,
        })
        .map(|g| g.map(|g| g.id))
        .sum()
}

fn part2<I: Iterator<Item = anyhow::Result<Game>>>(games: I) -> anyhow::Result<usize> {
    games
        .map(|g| {
            g.and_then(|g| {
                g.subsets
                    .into_iter()
                    .reduce(|acc, subset| Subset {
                        red: u32::max(acc.red, subset.red),
                        green: u32::max(acc.green, subset.green),
                        blue: u32::max(acc.blue, subset.blue),
                    })
                    .ok_or_else(|| anyhow!("expected at least one subset of the cubes"))
            })
        })
        .map(|cubes| cubes.map(|Subset { red, green, blue }| red * green * blue))
        .sum::<Result<u32, _>>()?
        .try_into()
        .context("result too big to fit in usize")
}

#[derive(Debug, Clone)]
struct Game {
    id: usize,
    subsets: Vec<Subset>,
}

#[derive(Debug, Clone, Copy, Default)]
struct Subset {
    red: u32,
    green: u32,
    blue: u32,
}

impl Game {
    fn from_line(line: &str) -> anyhow::Result<Self> {
        let remaining = expect("Game ", line)?;
        let (id, remaining) = integer(remaining)?;
        let remaining = expect(": ", remaining)?;

        let subsets = remaining
            .split("; ")
            .map(Subset::parse)
            .collect::<anyhow::Result<_>>()?;

        Ok(Self {
            id: id.try_into()?,
            subsets,
        })
    }
}

impl Subset {
    fn parse(mut s: &str) -> anyhow::Result<Self> {
        let (mut red, mut green, mut blue) = (0, 0, 0);

        loop {
            let ((value, c), remaining) = Self::color(s)?;

            match c {
                "red" => red = value,
                "green" => green = value,
                "blue" => blue = value,
                _ => unreachable!(),
            };

            match remaining.strip_prefix(", ") {
                None => break,
                Some(remaining) => s = remaining,
            };
        }

        Ok(Self { red, green, blue })
    }

    fn color(input: &str) -> anyhow::Result<((u32, &str), &str)> {
        let (value, remaining) = integer(input)?;
        let remaining = expect(" ", remaining)?;
        let sep_point = remaining.find([',', ';']).unwrap_or(remaining.len());
        let (color, remaining) = remaining.split_at(sep_point);

        match color {
            "red" | "green" | "blue" => Ok(((value, color), remaining)),
            _ => Err(anyhow!(r#"unknown color "{color}""#)),
        }
    }
}

fn integer(input: &str) -> anyhow::Result<(u32, &str)> {
    let digits = input.chars().take_while(char::is_ascii_digit).count();
    if digits > 0 {
        let int_part = &input[..digits];
        let res = int_part.parse().expect("all characters are digits");
        Ok((res, &input[digits..]))
    } else {
        Err(expected_err("<integer>", input))
    }
}

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let stdin = std::io::stdin().lock();
    let games = stdin.lines().map(|line| {
        line.context("error reading line")
            .and_then(|line| Game::from_line(&line))
    });

    run(part, part1, part2, games)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = concat!(
        "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n",
        "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n",
        "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n",
        "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n",
        "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green\n",
    );

    #[test]
    fn example_test_part1() {
        let games = TEST_INPUT.lines().map(Game::from_line);

        let result = part1(games).unwrap();
        assert_eq!(8, result);
    }

    #[test]
    fn example_test_part2() {
        let games = TEST_INPUT.lines().map(Game::from_line);

        let result = part2(games).unwrap();
        assert_eq!(2286, result);
    }
}
