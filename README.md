## Rust solutions to Advent of Code 2023

To run a specific day execute the binary `day-XY` providing the part (`1` or `2`) as the first argument and pipe the input data to its stdin.

For example to run part 2 of the day 3 with the input in the file `input` do:

```sh
cat input | cargo run --bin day-03 -- 2
```

### Benchmarks

Some solutions have benchmarks included. Those require an `input` file in their `benches` directory, and the benchmarks also assert the correct
result **for my input** (not included) – so if you want to run the benchmarks you need to copy your input file as `day-XY/benches/input` and then
fix the values in `assert_eq!()` invocations in the `day-XY/benches/benchmarks.rs` file.

Then you can run the benchmarks with:

```sh
cargo bench
```

or to run benchmarks only for a specific day – eg. day 20, do:

```sh
cargo bench --bench benchmarks --package day-20
```
