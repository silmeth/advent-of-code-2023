use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_23::*;

static INPUT: &str = include_str!("input");

fn parse_benchmark(c: &mut Criterion) {
    c.bench_function("day 23, parsing", |b| {
        b.iter(|| black_box(INPUT).parse::<IslandMap>().unwrap())
    });
}

fn part1_benchmark(c: &mut Criterion) {
    let map = INPUT.parse().unwrap();
    c.bench_function("day 23, part 1", |b| {
        b.iter(|| {
            assert_eq!(2366, part1(black_box(&map)));
        })
    });
}

criterion_group!(benches, parse_benchmark, part1_benchmark);
criterion_main!(benches);
