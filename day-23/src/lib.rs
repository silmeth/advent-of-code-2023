use std::{
    collections::{HashMap, HashSet},
    str::FromStr,
};

use anyhow::{anyhow, Context as _};
use smallvec::SmallVec;

// based on code copied from day 21
#[derive(Clone)]
pub struct IslandMap {
    tiles: Vec<Tile>,
    width: usize,
    height: usize,
}

impl IslandMap {
    fn is_in_bounds(&self, x: usize, y: usize) -> bool {
        x < self.width && y < self.height
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Tile {
    Path,
    Forest,
    Slope(Direction),
}

impl std::ops::Index<MapCoord> for IslandMap {
    type Output = Tile;

    fn index(&self, MapCoord { x, y }: MapCoord) -> &Self::Output {
        assert!(self.is_in_bounds(x, y));
        &self.tiles[y * self.width + x]
    }
}

impl std::ops::Index<usize> for IslandMap {
    type Output = [Tile];

    fn index(&self, y: usize) -> &Self::Output {
        assert!(y < self.height);
        let y = y * self.width;
        &self.tiles[y..y + self.width]
    }
}

impl FromStr for IslandMap {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut width: Option<usize> = None;
        let mut height: usize = 0;

        let tiles = s
            .as_bytes()
            .iter()
            .enumerate()
            .filter_map(|(idx, b)| match b {
                b'.' => Some(Ok(Tile::Path)),
                b'#' => Some(Ok(Tile::Forest)),
                b'>' => Some(Ok(Tile::Slope(Direction::E))),
                b'<' => Some(Ok(Tile::Slope(Direction::W))),
                b'v' => Some(Ok(Tile::Slope(Direction::S))),
                b'^' => Some(Ok(Tile::Slope(Direction::N))),
                b'\n' => {
                    if let Some(width) = width {
                        if (idx + 1) % (width + 1) != 0 {
                            Some(Err(anyhow!("expected all lines to be of equal length")))
                        } else {
                            height += 1;
                            None
                        }
                    } else {
                        height += 1;
                        width = Some(idx);
                        None
                    }
                }
                _ => Some(
                    std::str::from_utf8(&[*b])
                        .with_context(|| format!("non-ASCII byte {}", *b))
                        .and_then(|character| Err(anyhow!("unexpected character '{}'", character))),
                ),
            })
            .collect::<anyhow::Result<_>>()?;

        Ok(Self {
            tiles,
            width: width.with_context(|| format!("expected at least one line, got {s}"))?,
            height,
        })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct MapCoord {
    x: usize,
    y: usize,
}

impl MapCoord {
    fn neighbours<'a>(
        &'a self,
        map: &'a IslandMap,
    ) -> impl Iterator<Item = (MapCoord, Direction)> + 'a {
        let self_tile = map[*self];
        [Direction::N, Direction::S, Direction::E, Direction::W]
            .into_iter()
            // slopes lead only in their own direction
            .filter(move |dir| match self_tile {
                Tile::Slope(slope_dir) => *dir == slope_dir,
                Tile::Path | Tile::Forest => true,
            })
            .map(|dir| (dir.move_coords(*self), dir))
            .filter(|(node, _)| {
                map.is_in_bounds(node.x, node.y)
                    && matches!(map[*node], Tile::Path | Tile::Slope(_))
            })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum Direction {
    N,
    W,
    E,
    S,
}

impl Direction {
    /// let’s just wrap the coords and not worry about usize::MAX-sized maps…
    pub fn move_coords(self, MapCoord { x, y }: MapCoord) -> MapCoord {
        let (x, y) = match self {
            Self::N => (x, y.wrapping_sub(1)),
            Self::E => (x.wrapping_add(1), y),
            Self::S => (x, y.wrapping_add(1)),
            Self::W => (x.wrapping_sub(1), y),
        };
        MapCoord { x, y }
    }

    pub fn invert(self) -> Self {
        match self {
            Direction::N => Direction::S,
            Direction::W => Direction::E,
            Direction::E => Direction::W,
            Direction::S => Direction::N,
        }
    }
}

fn find_longest_path(map: &IslandMap) -> u64 {
    let start_x = map[0].iter().position(|tile| *tile == Tile::Path).unwrap();
    let current = MapCoord { x: start_x, y: 0 };

    let neighbours_graph = convert_map(map);

    find_longest_path_recursive(&neighbours_graph, current, &HashSet::new(), map.height - 1)
        .unwrap()
}

fn find_longest_path_recursive(
    graph: &HashMap<MapCoord, Neighbours>,
    current: MapCoord,
    visited: &HashSet<MapCoord>,
    end_y: usize,
) -> Option<u64> {
    if current.y == end_y {
        Some(0)
    } else {
        let visited = {
            let mut visited = visited.clone();
            visited.insert(current);
            visited
        };
        let neighbours = &graph[&current];

        neighbours
            .iter()
            .filter(|n| !visited.contains(&n.id))
            .filter_map(|neighbour| {
                find_longest_path_recursive(graph, neighbour.id, &visited, end_y)
                    .map(|it| it + neighbour.distance)
            })
            .max()
    }
}

#[derive(Debug, PartialEq, Eq)]
struct GraphNeighbour {
    distance: u64,
    id: MapCoord,
}

type Neighbours = SmallVec<[GraphNeighbour; 4]>;

fn convert_map(map: &IslandMap) -> HashMap<MapCoord, Neighbours> {
    let start_x = map[0].iter().position(|tile| *tile == Tile::Path).unwrap();
    let start = MapCoord { x: start_x, y: 0 };

    let mut graph = HashMap::new();
    gather_neighbours(map, start, Direction::S, &mut graph);
    graph
}

fn gather_neighbours(
    map: &IslandMap,
    previous: MapCoord,
    mut direction: Direction,
    graph: &mut HashMap<MapCoord, Neighbours>,
) {
    let mut current = direction.move_coords(previous);
    let mut distance = 1;
    let mut neighbours;
    // if no of the tiles along the path are slopes, the neighbour
    // relation works both ways, if there is a slope it does not
    // commute
    let mut commutes = !matches!(map[current], Tile::Slope(_));

    loop {
        // all neighbours except for the one we came from
        neighbours = current
            .neighbours(map)
            .filter(|(_, d)| *d != direction.invert())
            .collect::<SmallVec<[(MapCoord, Direction); 3]>>();

        if neighbours.is_empty() && current.y != map.height - 1 {
            // dead end
            return;
        }

        if neighbours.len() >= 2 || current.y == map.height - 1 {
            // a cross-road or end found!
            break;
        }

        distance += 1;
        (current, direction) = *neighbours
            .iter()
            .find(|(_, d)| *d != direction.invert())
            .unwrap();
        commutes = commutes && !matches!(map[current], Tile::Slope(_));
    }

    {
        // add this neighbour to previous
        let previous_neighbours = graph.entry(previous).or_default();
        let this_neighbour = GraphNeighbour {
            id: current,
            distance,
        };
        if !previous_neighbours.contains(&this_neighbour) {
            previous_neighbours.push(this_neighbour);
        }
    }

    // check if the node has already been visited – if not, we need to go deeper
    let recurse_further = !graph.contains_key(&current);

    if commutes {
        // add previous as a neighbour to this node
        let current_neighbours = graph.entry(current).or_default();
        let previous_neighbour = GraphNeighbour {
            id: previous,
            distance,
        };
        if !current_neighbours.contains(&previous_neighbour) {
            current_neighbours.push(previous_neighbour);
        }
    }

    if recurse_further {
        for (_, d) in neighbours {
            gather_neighbours(map, current, d, graph);
        }
    }
}

pub fn part1(map: &IslandMap) -> u64 {
    find_longest_path(map)
}

pub fn part2(map: &IslandMap) -> u64 {
    let mut map = map.clone();
    for tile in &mut map.tiles {
        if matches!(tile, Tile::Slope(_)) {
            *tile = Tile::Path;
        }
    }

    find_longest_path(&map)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "#.#####################\n",
        "#.......#########...###\n",
        "#######.#########.#.###\n",
        "###.....#.>.>.###.#.###\n",
        "###v#####.#v#.###.#.###\n",
        "###.>...#.#.#.....#...#\n",
        "###v###.#.#.#########.#\n",
        "###...#.#.#.......#...#\n",
        "#####.#.#.#######.#.###\n",
        "#.....#.#.#.......#...#\n",
        "#.#####.#.#.#########v#\n",
        "#.#...#...#...###...>.#\n",
        "#.#.#v#######v###.###v#\n",
        "#...#.>.#...>.>.#.###.#\n",
        "#####v#.#.###v#.#.###.#\n",
        "#.....#...#...#.#.#...#\n",
        "#.#########.###.#.#.###\n",
        "#...###...#...#...#.###\n",
        "###.###.#.###v#####v###\n",
        "#...#...#.#.>.>.#.>.###\n",
        "#.###.###.#.###.#.#v###\n",
        "#.....###...###...#...#\n",
        "#####################.#\n",
    );

    #[test]
    fn test_part1() {
        let map = INPUT.parse().unwrap();
        let res = part1(&map);
        assert_eq!(94, res);
    }

    #[test]
    fn test_part2() {
        let map = INPUT.parse().unwrap();
        let res = part2(&map);
        assert_eq!(154, res);
    }
}
