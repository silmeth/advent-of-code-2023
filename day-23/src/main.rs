use day_23::{part1, part2, IslandMap};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let map: IslandMap = utils::read_entire_stdin()?.parse()?;

    run(part, |map| Ok(part1(map)), |map| Ok(part2(map)), &map)
}
