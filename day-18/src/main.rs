use day_18::{part1, part2, Instructions};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let instructions: Instructions = utils::read_entire_stdin()?.parse()?;

    run(part, part1, part2, &instructions)
}
