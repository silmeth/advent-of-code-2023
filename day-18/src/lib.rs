use std::{ops::Deref, str::FromStr};

use anyhow::{anyhow, Context as _};
use utils::{ascii_digit_to_num, expect_ascii_byte, expected_err};

pub struct Instructions(Vec<Instruction>);

impl Deref for Instructions {
    type Target = [Instruction];

    fn deref(&self) -> &Self::Target {
        &self.0[..]
    }
}

impl FromStr for Instructions {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut res = Vec::new();
        let mut input = s.as_bytes();
        while !input.is_empty() {
            let (instruction, rem) = Self::parse_instruction(input)?;
            input = rem;
            res.push(instruction);
        }

        Ok(Self(res))
    }
}

impl Instructions {
    fn parse_instruction(input: &[u8]) -> anyhow::Result<(Instruction, &[u8])> {
        let (direction, input) = Self::parse_direction(input)?;

        let input = expect_ascii_byte(b' ', input)?;

        let (length, input) = Self::parse_int(input)?;

        let input = expect_ascii_byte(b' ', input)?;
        let input = expect_ascii_byte(b'(', input)?;

        let (color, input) = Self::parse_color(input)?;

        let input = expect_ascii_byte(b')', input)?;
        let input = if input[0] == b'\r' {
            expect_ascii_byte(b'\n', &input[1..])
        } else {
            expect_ascii_byte(b'\n', input)
        }?;

        Ok((
            Instruction {
                direction,
                length,
                color,
            },
            input,
        ))
    }

    fn parse_direction(input: &[u8]) -> anyhow::Result<(Direction, &[u8])> {
        let dir = match input[0] {
            b'R' => Direction::Right,
            b'D' => Direction::Down,
            b'L' => Direction::Left,
            b'U' => Direction::Up,
            dir => {
                return Err(anyhow!(
                    "unknown direction '{}'",
                    std::str::from_utf8(&[dir])
                        .with_context(|| format!("non-ASCII byte '{dir}'"))?
                ))
            }
        };

        Ok((dir, &input[1..]))
    }

    fn parse_color(input: &[u8]) -> anyhow::Result<(Color, &[u8])> {
        let input = expect_ascii_byte(b'#', input)?;
        let (r, input) = Self::parse_hex_byte(input)?;
        let (g, input) = Self::parse_hex_byte(input)?;
        let (b, input) = Self::parse_hex_byte(input)?;
        Ok((Color { r, g, b }, input))
    }

    fn parse_int(mut input: &[u8]) -> anyhow::Result<(u64, &[u8])> {
        if input.is_empty() || !input[0].is_ascii_digit() {
            return Err(anyhow!(
                "expected integer, got '{}'",
                std::str::from_utf8(input)
                    .with_context(|| format!("non unicode bytes: {:?}", input))?
            ));
        }

        let mut res = 0;
        while !input.is_empty() && input[0].is_ascii_digit() {
            res *= 10;
            res += u64::from(ascii_digit_to_num(input[0]));
            input = &input[1..];
        }

        Ok((res, input))
    }

    fn parse_hex_byte(input: &[u8]) -> anyhow::Result<(u8, &[u8])> {
        let byte_to_hex = |byte| match byte {
            b'0'..=b'9' => Ok(byte - b'0'),
            b'a'..=b'f' => Ok(10 + byte - b'a'),
            b'A'..=b'F' => Ok(10 + byte - b'A'),
            _ => Err(expected_err(
                "[hex digit]",
                std::str::from_utf8(&[byte]).with_context(|| format!("non-ASCII byte {}", byte))?,
            )),
        };

        if input.len() < 2 {
            return Err(anyhow!("expected 2 hex digits"));
        }

        let hex = byte_to_hex(input[0])? * 16 + byte_to_hex(input[1])?;
        Ok((hex, &input[2..]))
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Instruction {
    direction: Direction,
    length: u64,
    color: Color,
}

impl Instruction {
    fn convert_to_part2(&self) -> anyhow::Result<Instruction> {
        let direction = self.color.b & 0x0f;
        let direction = match direction {
            0 => Direction::Right,
            1 => Direction::Down,
            2 => Direction::Left,
            3 => Direction::Up,
            _ => return Err(anyhow!("wrong encoded direction!")),
        };

        let length = u64::from(self.color.b >> 4)
            | (u64::from(self.color.g) << 4)
            | (u64::from(self.color.r) << (3 * 4));

        Ok(Self {
            color: self.color,
            direction,
            length,
        })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Direction {
    Right,
    Down,
    Left,
    Up,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Color {
    r: u8,
    g: u8,
    b: u8,
}

// Green’s Theorem reduced to discrete-points polygon
pub fn interior_area<I: Iterator<Item = anyhow::Result<Instruction>>>(
    mut instructions: I,
) -> anyhow::Result<u64> {
    let (mut x, mut y): (i64, i64) = (0, 0);
    let (double_area, total_length) = instructions.try_fold((0, 0), |(area2, tot_len), ins| {
        let ins = ins?;
        let (new_x, new_y) = match ins.direction {
            Direction::Right => (x.wrapping_add(ins.length as i64), y),
            Direction::Down => (x, y.wrapping_add(ins.length as i64)),
            Direction::Left => (x.wrapping_sub(ins.length as i64), y),
            Direction::Up => (x, y.wrapping_sub(ins.length as i64)),
        };

        let new_acc = area2 + x * new_y - y * new_x;
        (x, y) = (new_x, new_y);
        Ok::<_, anyhow::Error>((new_acc, tot_len + ins.length))
    })?;

    Ok((i64::abs(double_area) as u64 + total_length) / 2 + 1)
}

pub fn part1(instructions: &Instructions) -> anyhow::Result<u64> {
    interior_area(instructions.iter().copied().map(anyhow::Result::Ok))
}

pub fn part2(instructions: &Instructions) -> anyhow::Result<u64> {
    interior_area(instructions.iter().map(Instruction::convert_to_part2))
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "R 6 (#70c710)\n",
        "D 5 (#0dc571)\n",
        "L 2 (#5713f0)\n",
        "D 2 (#d2c081)\n",
        "R 2 (#59c680)\n",
        "D 2 (#411b91)\n",
        "L 5 (#8ceee2)\n",
        "U 2 (#caa173)\n",
        "L 1 (#1b58a2)\n",
        "U 2 (#caa171)\n",
        "R 2 (#7807d2)\n",
        "U 3 (#a77fa3)\n",
        "L 2 (#015232)\n",
        "U 2 (#7a21e3)\n",
    );

    #[test]
    fn test_parsing() {
        let result: Instructions = INPUT.parse().unwrap();
        assert_eq!(14, result.len());
    }

    #[test]
    fn test_part1() {
        let instructions: Instructions = INPUT.parse().unwrap();
        assert_eq!(62, part1(&instructions).unwrap());
    }

    #[test]
    fn test_part2() {
        let instructions: Instructions = INPUT.parse().unwrap();
        assert_eq!(952408144115, part2(&instructions).unwrap());
    }
}
