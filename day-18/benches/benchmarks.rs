use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_18::*;

static INPUT: &str = include_str!("input");

fn parse_benchmark(c: &mut Criterion) {
    c.bench_function("day 18, parsing", |b| {
        b.iter(|| black_box(INPUT).parse::<Instructions>().unwrap())
    });
}

fn part1_benchmark(c: &mut Criterion) {
    let instructions = INPUT.parse::<Instructions>().unwrap();
    c.bench_function("day 18, part 1", |b| {
        b.iter(|| assert_eq!(28911, part1(black_box(&instructions)).unwrap()))
    });
}

fn part2_benchmark(c: &mut Criterion) {
    let instructions = INPUT.parse::<Instructions>().unwrap();
    c.bench_function("day 18, part 2", |b| {
        b.iter(|| assert_eq!(77366737561114, part2(black_box(&instructions)).unwrap()))
    });
}

criterion_group!(benches, parse_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);
