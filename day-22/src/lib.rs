use std::collections::{HashMap, HashSet};

use anyhow::Context as _;
use utils::{ascii_digit_to_num, expect_ascii_byte};

#[derive(Clone)]
pub struct RectCuboid {
    p1: (usize, usize, usize),
    p2: (usize, usize, usize),
}

impl RectCuboid {
    pub fn lower(&self) -> usize {
        usize::min(self.p1.2, self.p2.2)
    }

    pub fn upper(&self) -> usize {
        usize::max(self.p1.2, self.p2.2)
    }

    pub fn left(&self) -> usize {
        usize::min(self.p1.0, self.p2.0)
    }

    pub fn right(&self) -> usize {
        usize::max(self.p1.0, self.p2.0)
    }

    /// lower y edge
    pub fn shallow(&self) -> usize {
        usize::min(self.p1.1, self.p2.1)
    }

    /// higher y edge
    pub fn deep(&self) -> usize {
        usize::max(self.p1.1, self.p2.1)
    }

    pub fn xs(&self) -> std::ops::Range<usize> {
        self.left()..self.right() + 1
    }

    pub fn ys(&self) -> std::ops::Range<usize> {
        self.shallow()..self.deep() + 1
    }
}

pub fn parse(input: &str) -> anyhow::Result<Vec<RectCuboid>> {
    let mut input = input.as_bytes();
    let mut res = Vec::new();

    type ParseRes<'a, T> = anyhow::Result<(&'a [u8], T)>;

    fn parse_num(mut input: &[u8]) -> ParseRes<usize> {
        let mut res: usize = usize::from(ascii_digit_to_num(
            *input.first().context("expected at least 1 digit")?,
        ));
        input = &input[1..];

        while let Some(b @ b'0'..=b'9') = input.first() {
            res *= 10;
            res += usize::from(ascii_digit_to_num(*b));
            input = &input[1..];
        }

        Ok((input, res))
    }

    fn parse_point(input: &[u8]) -> ParseRes<(usize, usize, usize)> {
        let (input, x) = parse_num(input)?;
        let input = expect_ascii_byte(b',', input)?;
        let (input, y) = parse_num(input)?;
        let input = expect_ascii_byte(b',', input)?;
        let (input, z) = parse_num(input)?;

        Ok((input, (x, y, z)))
    }

    while !input.is_empty() {
        let (x, y, z);
        (input, (x, y, z)) = parse_point(input)?;
        input = expect_ascii_byte(b'~', input)?;
        let (x2, y2, z2);
        (input, (x2, y2, z2)) = parse_point(input)?;
        input = expect_ascii_byte(b'\n', input)?;

        res.push(RectCuboid {
            p1: (x, y, z),
            p2: (x2, y2, z2),
        })
    }

    Ok(res)
}

struct HeightMap {
    map: Vec<Vec<(usize, usize)>>,
    width: usize,
    depth: usize,
}

impl HeightMap {
    fn iter_mut_region(
        &mut self,
        xs: std::ops::Range<usize>,
        ys: std::ops::Range<usize>,
    ) -> HeightMapRegionIterator {
        let x_start = xs.start;
        HeightMapRegionIterator {
            map: self,
            xs,
            y_bound: ys.end,
            x: x_start,
            y: ys.start,
        }
    }
}

impl std::ops::Index<(usize, usize)> for HeightMap {
    type Output = Vec<(usize, usize)>;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        assert!(x < self.width && y < self.depth);
        &self.map[y * self.width + x]
    }
}

impl std::ops::IndexMut<(usize, usize)> for HeightMap {
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut Self::Output {
        assert!(x < self.width && y < self.depth);
        &mut self.map[y * self.width + x]
    }
}

struct HeightMapRegionIterator<'a> {
    map: &'a mut HeightMap,
    xs: std::ops::Range<usize>,
    y_bound: usize,
    x: usize,
    y: usize,
}

impl<'a> Iterator for HeightMapRegionIterator<'a> {
    type Item = &'a mut Vec<(usize, usize)>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.x >= self.xs.end {
            self.x = self.xs.start;
            self.y += 1;
        }
        if self.y >= self.y_bound {
            return None;
        }

        let x = self.x;
        self.x += 1;
        // SAFETY: we know that the returned references are non-overlapping and not related
        // to the lifetime of the iterator object itself (but to the lifetime of the `self.map`
        // itself
        unsafe { Some(&mut *(&mut self.map[(x, self.y)] as *mut _)) }
    }
}

/// returns a vector of sets of indices of the cuboids that support the nth one
fn move_to_ground(cuboids: &mut [RectCuboid]) -> Vec<HashSet<usize>> {
    // let’s sort the input by the higher z-value, by their top sides going from ground up
    cuboids.sort_by_key(|cuboid| cuboid.upper());

    let (pile_width, pile_depth) = cuboids
        .iter()
        .fold((0, 0), |(x, y), c| (x.max(c.right()), y.max(c.deep())));
    let pile_width = pile_width + 1;
    let pile_depth = pile_depth + 1;

    let mut height_map = HeightMap {
        map: vec![vec![]; pile_width * pile_depth],
        width: pile_width,
        depth: pile_depth,
    };

    // indices of cuboids that support this one
    let mut supported_by = vec![HashSet::new(); cuboids.len()];

    // bring them as low as possible
    for ((cuboid_idx, cuboid), supp_by) in
        cuboids.iter_mut().enumerate().zip(supported_by.iter_mut())
    {
        let support_level = height_map
            .iter_mut_region(cuboid.xs(), cuboid.ys())
            .map(|h| h.last().map(|(_, h)| *h).unwrap_or(0))
            .max()
            .unwrap();
        let target_level = support_level + 1;
        let z_diff = cuboid.lower() - target_level;
        cuboid.p1.2 -= z_diff;
        cuboid.p2.2 -= z_diff;
        let new_level = cuboid.upper();

        for cuboids_below in height_map.iter_mut_region(cuboid.xs(), cuboid.ys()) {
            if let Some((idx, h)) = cuboids_below.last() {
                if *h == support_level {
                    supp_by.insert(*idx);
                }
            }
            cuboids_below.push((cuboid_idx, new_level));
        }
    }

    supported_by
}

pub fn part1(mut cuboids: Vec<RectCuboid>) -> usize {
    let supported_by = move_to_ground(&mut cuboids);

    // all the cuboids that are the sole supporters of any other cuboid
    let impossible_to_remove: HashSet<usize> = supported_by
        .iter()
        .filter_map(|supporters| {
            if supporters.len() == 1 {
                Some(*supporters.iter().next().unwrap())
            } else {
                None
            }
        })
        .collect();

    cuboids.len() - impossible_to_remove.len()
}

pub fn part2(mut cuboids: Vec<RectCuboid>) -> anyhow::Result<usize> {
    let supported_by = move_to_ground(&mut cuboids);
    let sole_suporters: HashMap<usize, HashSet<usize>> = supported_by
        .iter()
        .enumerate()
        .filter(|(_, supporters)| supporters.len() == 1)
        .fold(HashMap::new(), |mut map, (idx, supporters)| {
            for supporter in supporters {
                map.entry(*supporter).or_default().insert(idx);
            }
            map
        });

    let mut res = 0;
    for (disintegrated, supported) in sole_suporters {
        let mut falling = HashSet::new();

        // the index of the first potentially affected cuboid above the ones directly affected.
        for cuboid in supported {
            falling.insert(cuboid);
        }

        // the cuboids are sorted from the bottom up, so if one’s
        // supporters get removed and we reach it, its supporters
        // are already inserted in `removed`
        for (idx, supporters) in supported_by[disintegrated + 1..].iter().enumerate() {
            if !supporters.is_empty() && supporters.iter().all(|it| falling.contains(it)) {
                falling.insert(idx + disintegrated + 1);
            }
        }

        res += falling.len();
    }

    Ok(res)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "1,0,1~1,2,1\n",
        "0,0,2~2,0,2\n",
        "0,2,3~2,2,3\n",
        "0,0,4~0,2,4\n",
        "2,0,5~2,2,5\n",
        "0,1,6~2,1,6\n",
        "1,1,8~1,1,9\n",
    );

    #[test]
    fn should_parse_input() {
        let result = parse(INPUT).unwrap();
        assert_eq!(7, result.len());
    }

    #[test]
    fn test_part1() {
        let cuboids = parse(INPUT).unwrap();
        let res = part1(cuboids);
        assert_eq!(5, res);
    }

    #[test]
    fn test_part2() {
        let cuboids = parse(INPUT).unwrap();
        let res = part2(cuboids).unwrap();
        assert_eq!(7, res);
    }
}
