use day_22::{parse, part1, part2};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let cuboids = parse(&utils::read_entire_stdin()?)?;

    run(part, |c| Ok(part1(c)), part2, cuboids)
}
