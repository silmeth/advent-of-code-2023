use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_22::*;
use std::time::{Duration, Instant};

static INPUT: &str = include_str!("input");

fn parse_benchmark(c: &mut Criterion) {
    c.bench_function("day 22, parsing", |b| {
        b.iter(|| parse(black_box(INPUT)).unwrap())
    });
}

fn perform_benchmark<R: for<'a> FnMut(Vec<RectCuboid>)>(
    name: &'static str,
    mut routine: R,
    c: &mut Criterion,
) {
    let input = parse(INPUT).unwrap();
    c.bench_function(name, |b| {
        b.iter_custom(|iters| {
            let mut time = Duration::ZERO;
            for _ in 0..iters {
                let input = input.clone();
                let start = Instant::now();
                routine(input);
                time += start.elapsed();
            }
            time
        })
    });
}

fn part1_benchmark(c: &mut Criterion) {
    perform_benchmark(
        "day 22, part 1",
        |input| assert_eq!(405, part1(black_box(input)),),
        c,
    );
}

fn part2_benchmark(c: &mut Criterion) {
    perform_benchmark(
        "day 22, part 2",
        |input| assert_eq!(61297, part2(black_box(input)).unwrap(),),
        c,
    );
}

criterion_group!(benches, parse_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);
