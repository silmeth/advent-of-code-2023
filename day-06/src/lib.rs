use std::str::FromStr;

use anyhow::anyhow;
use anyhow::Context as _;

pub fn part1(input: &Input) -> anyhow::Result<u64> {
    input
        .races1()
        .map(|(time, min_dist)| {
            calculate_press_time(time, min_dist).map(|range| range.end - range.start)
        })
        .try_fold(1, |acc, res| res.map(|num_wins| acc * num_wins))
}

pub fn part2(input: &Input) -> anyhow::Result<u64> {
    let (time, min_dist) = input.race2();
    let range = calculate_press_time(time, min_dist)?;
    Ok(range.end - range.start)
}

fn calculate_press_time(
    available_time: u64,
    min_dist: u64,
) -> anyhow::Result<std::ops::Range<u64>> {
    use std::cmp::Ordering;
    let available_time: f64 = available_time as f64;
    let min_dist: f64 = min_dist as f64;

    let root_factor = (available_time * available_time - (4.0 * min_dist)).sqrt();

    if root_factor.is_nan() {
        return Err(anyhow!("no real solution"));
    }

    let time1 = (root_factor + available_time) / 2.0;
    let time2 = (-root_factor + available_time) / 2.0;

    match time1.partial_cmp(&time2) {
        Some(Ordering::Less) => Ok((time1.floor() as u64 + 1)..time2.ceil() as u64),
        Some(Ordering::Greater) => Ok((time2.floor() as u64 + 1)..time1.ceil() as u64),
        Some(Ordering::Equal) => Ok((time1.floor() as u64 + 1)..time1.ceil() as u64 + 1),
        None => Err(anyhow!("failed to produce solution")),
    }
}

pub struct Input {
    times1: Vec<u64>,
    min_dists1: Vec<u64>,
    time2: u64,
    min_dist2: u64,
}

impl Input {
    pub fn races1(&self) -> impl Iterator<Item = (u64, u64)> + '_ {
        self.times1
            .iter()
            .cloned()
            .zip(self.min_dists1.iter().cloned())
    }

    pub fn race2(&self) -> (u64, u64) {
        (self.time2, self.min_dist2)
    }
}

impl FromStr for Input {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (times, min_dists) = s
            .split_once('\n')
            .map(|(a, b)| (a.trim(), b.trim()))
            .context("expected 2 lines in the input")?;

        let times: Vec<_> = times
            .strip_prefix("Time:")
            .context(r#"expected "Times:""#)?
            .split_ascii_whitespace()
            .collect();

        let min_dists: Vec<_> = min_dists
            .strip_prefix("Distance:")
            .context(r#"expected "Times:""#)?
            .split_ascii_whitespace()
            .collect();

        let times1 = times
            .iter()
            .map(|it| it.parse())
            .collect::<Result<_, _>>()
            .context("expected whitespace-separated times list")?;

        let min_dists1 = min_dists
            .iter()
            .map(|it| it.parse())
            .collect::<Result<_, _>>()
            .context("expected whitespace-separated distance list")?;

        let time2 = times.join("").parse().context("Time out of u64 range")?;
        let min_dist2 = min_dists
            .join("")
            .parse()
            .context("Distance out of u64 range")?;

        Ok(Self {
            times1,
            min_dists1,
            time2,
            min_dist2,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = concat!("Time:      7  15   30\n", "Distance:  9  40  200\n",);

    #[test]
    fn example_input_part1() {
        let result = part1(&TEST_INPUT.parse().unwrap()).unwrap();
        assert_eq!(288, result);
    }

    #[test]
    fn example_input_part2() {
        let result = part2(&TEST_INPUT.parse().unwrap()).unwrap();
        assert_eq!(71503, result);
    }
}
