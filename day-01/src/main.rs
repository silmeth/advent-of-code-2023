use regex::Regex;
use std::{io::BufRead, sync::OnceLock};
use utils::run;

fn part1<I: BufRead>(input: I) -> u32 {
    input
        .lines()
        .map(|line| {
            let line = line.expect("could not read line");
            let mut digits = line.chars().filter_map(|ch| ch.to_digit(10));
            let first = digits.next().expect("first digit");
            let last = digits.last().unwrap_or(first);
            first * 10 + last
        })
        .sum()
}

fn part2<I: BufRead>(input: I) -> u32 {
    input
        .lines()
        .map(|line| {
            let line = line.expect("could not read line");
            let mut digits = Digits { s: &line };
            let first = str_to_digit(digits.next().expect("first digit"));
            let last = digits.last().map(str_to_digit).unwrap_or(first);
            first * 10 + last
        })
        .sum()
}

fn str_to_digit(s: &str) -> u32 {
    match s {
        "one" => 1,
        "two" => 2,
        "three" => 3,
        "four" => 4,
        "five" => 5,
        "six" => 6,
        "seven" => 7,
        "eight" => 8,
        "nine" => 9,
        _ => s.parse().unwrap(),
    }
}

struct Digits<'a> {
    s: &'a str,
}

impl<'a> Iterator for Digits<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<Self::Item> {
        static DIGITS_REGEX: OnceLock<Regex> = OnceLock::new();
        let regex = DIGITS_REGEX.get_or_init(|| {
            Regex::new("^(?:one|two|three|four|five|six|seven|eight|nine|[0-9])").unwrap()
        });

        let result = loop {
            if self.s.is_empty() {
                break None;
            }

            let current = self.s;
            self.s = &self.s[1..];

            if let Some(digit) = regex.find(current) {
                break Some(digit.as_str());
            }
        };

        result
    }
}

fn main() {
    let part = std::env::args().try_into().unwrap();

    let stdin = std::io::stdin().lock();

    run(
        part,
        |stdin| Ok(part1(stdin)),
        |stdin| Ok(part2(stdin)),
        stdin,
    )
    .unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example_test_part1() {
        let input = concat!("1abc2\n", "pqr3stu8vwx\n", "a1b2c3d4e5f\n", "treb7uchet\n",);

        let result = part1(&mut input.as_bytes());
        assert_eq!(142, result);
    }

    #[test]
    fn example_test_part2() {
        let input = concat!(
            "two1nine\n",
            "eightwothree\n",
            "abcone2threexyz\n",
            "xtwone3four\n",
            "4nineeightseven2\n",
            "zoneight234\n",
            "7pqrstsixteen\n",
        );

        let result = part2(&mut input.as_bytes());
        assert_eq!(281, result);
    }
}
