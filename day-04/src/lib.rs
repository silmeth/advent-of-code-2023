use anyhow::anyhow;
use anyhow::Context as _;
use std::collections::HashSet;
use utils::expected_err;

pub fn part1<I: Iterator<Item = anyhow::Result<Card>>>(cards: I) -> anyhow::Result<u32> {
    cards.map(|card| card.map(|card| card.score())).sum()
}

pub fn part2<I: Iterator<Item = anyhow::Result<Card>>>(mut cards: I) -> anyhow::Result<u32> {
    let mut duplicates = Vec::<u32>::with_capacity(256);
    cards.try_fold(0, move |acc, card| match card {
        Ok(card) => {
            let self_dups = match duplicates.get_mut(card.id - 1) {
                Some(dups) => *dups + 1,
                None => {
                    duplicates.push(0);
                    1
                }
            };

            let matches = card.matches();

            for idx in card.id..card.id + usize::try_from(matches).context("too many matches")? {
                match duplicates.get_mut(idx) {
                    Some(dups) => *dups += self_dups,
                    None => duplicates.push(self_dups),
                }
            }

            Ok(acc + self_dups)
        }
        Err(e) => Err(e),
    })
}

#[derive(Debug, Clone)]
pub struct Card {
    pub id: usize,
    pub winners: HashSet<u8>,
    pub guesses: Vec<u8>,
}

impl Card {
    pub fn try_from_line((idx, line): (usize, &str)) -> anyhow::Result<Self> {
        let remaining = line
            .strip_prefix("Card ")
            .ok_or_else(|| expected_err("Card ", line))?;
        let (id, remaining) = remaining
            .split_once(": ")
            .ok_or_else(|| expected_err("<num>: ", remaining))?;
        let id = id.trim().parse().context("expected card id")?;

        if idx != id - 1 {
            return Err(anyhow!("cards in the input must be sorted"));
        }

        let (winners, guesses) = remaining
            .split_once(" | ")
            .ok_or_else(|| expected_err("<num list> | <num list>", remaining))?;

        let winners = winners
            .split(' ')
            .filter(|it| !it.is_empty())
            .map(|it| it.parse().context("expected number"))
            .collect::<anyhow::Result<_>>()?;

        let guesses = guesses
            .split(' ')
            .filter(|it| !it.is_empty())
            .map(|it| it.parse().context("expected number"))
            .collect::<anyhow::Result<_>>()?;

        Ok(Self {
            id,
            winners,
            guesses,
        })
    }

    pub fn matches(&self) -> u32 {
        self.guesses
            .iter()
            .filter(|guess| self.winners.contains(guess))
            .count()
            .try_into()
            .expect("number of guesses does not fit in u32")
    }

    pub fn score(&self) -> u32 {
        let correct_guesses: u32 = self.matches();
        if correct_guesses == 0 {
            0
        } else {
            2_u32.pow(correct_guesses - 1)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\n",
        "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\n",
        "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\n",
        "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\n",
        "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\n",
        "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11\n",
    );

    #[test]
    fn example_input_part1() {
        let result = part1(INPUT.lines().enumerate().map(Card::try_from_line)).unwrap();
        assert_eq!(13, result);
    }

    #[test]
    fn example_input_part2() {
        let result = part2(INPUT.lines().enumerate().map(Card::try_from_line)).unwrap();
        assert_eq!(30, result);
    }
}
