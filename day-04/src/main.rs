use std::io::BufRead as _;

use anyhow::Context as _;
use day_04::Card;
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let stdin = std::io::stdin().lock();
    let cards = stdin.lines().enumerate().map(|(idx, line)| {
        line.context("Failed to read line")
            .and_then(|line| Card::try_from_line((idx, &line)))
    });

    run(part, day_04::part1, day_04::part2, cards)
}
