use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_04::*;

static INPUT: &str = include_str!("input");

fn part2_benchmark(c: &mut Criterion) {
    c.bench_function("day 4, part 2", |b| {
        b.iter(|| {
            assert_eq!(
                9924412,
                part2(
                    black_box(INPUT)
                        .lines()
                        .enumerate()
                        .map(Card::try_from_line)
                )
                .unwrap()
            )
        })
    });
}

criterion_group!(benches, part2_benchmark);
criterion_main!(benches);
