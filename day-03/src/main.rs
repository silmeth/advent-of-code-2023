use std::collections::HashSet;
use std::io::Read;
use std::ops::Index;
use std::ops::Range;

use anyhow::anyhow;
use utils::run;

fn part1(schema: EngineSchema<'_>) -> u32 {
    let part_numbers = schema.numbers().collect::<Vec<_>>();
    // prevent flat_map closure from taking ownership
    let part_numbers = &part_numbers;

    schema
        .raw_schema
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            line.char_indices()
                .filter(|(_, ch)| !(*ch == '.' || ch.is_ascii_digit()))
                .flat_map(move |(x, _)| part_numbers.iter().filter(move |part| part.touches(x, y)))
        })
        // make unique
        .collect::<HashSet<_>>()
        .into_iter()
        .map(|part| part.value)
        .sum()
}

fn part2(schema: EngineSchema<'_>) -> u32 {
    let part_numbers = schema.numbers().collect::<Vec<_>>();
    // prevent filter_map closure from taking ownership
    let part_numbers = &part_numbers;

    schema
        .raw_schema
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            line.char_indices()
                .filter(|(_, ch)| *ch == '*')
                .filter_map(move |(x, _)| {
                    let touching_parts = part_numbers
                        .iter()
                        .filter(|part| part.touches(x, y))
                        .collect::<Vec<_>>();
                    if touching_parts.len() == 2 {
                        Some(touching_parts.iter().fold(1, |acc, part| acc * part.value))
                    } else {
                        None
                    }
                })
        })
        .sum()
}

struct EngineSchema<'a> {
    raw_schema: &'a str,
    width: usize,
    height: usize,
}

impl<'a> EngineSchema<'a> {
    fn try_new(input: &'a str) -> anyhow::Result<Self> {
        let width = input.find('\n').unwrap_or(input.len());
        // line has `width` characters + '\n'; let’s require all lines end in `\n`
        let height = input.len() / (width + 1);

        // ensure the input is a valid rectangle
        if (width + 1) * height != input.len() {
            return Err(anyhow!("input must have all lines of equal length!"));
        }
        if !input
            .char_indices()
            .filter_map(|(idx, ch)| {
                if idx + 1 % (width + 1) == 0 {
                    Some(ch)
                } else {
                    None
                }
            })
            .all(|ch| ch == '\n')
        {
            return Err(anyhow!("input must have all lines of equal length!"));
        }

        Ok(Self {
            raw_schema: input,
            width,
            height,
        })
    }

    fn numbers(&self) -> impl Iterator<Item = PartNumber> + '_ {
        self.raw_schema.lines().enumerate().flat_map(|(y, line)| {
            LineNums::new(line).map(move |(range, num)| PartNumber {
                value: num,
                line: y,
                xs: range,
            })
        })
    }
}

impl<'a> Index<(usize, Range<usize>)> for EngineSchema<'a> {
    type Output = str;

    fn index(&self, (y, xs): (usize, Range<usize>)) -> &Self::Output {
        assert!(xs.end <= self.width && y <= self.height);
        let offset = y * (self.width + 1);

        &self.raw_schema[offset + xs.start..offset + xs.end]
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
struct PartNumber {
    value: u32,
    line: usize,
    xs: Range<usize>,
}

impl PartNumber {
    fn touches(&self, x: usize, y: usize) -> bool {
        let min_line = self.line.saturating_sub(1);
        let min_x = self.xs.start.saturating_sub(1);
        (y >= min_line && y <= self.line + 1) && (x >= min_x && x <= self.xs.end)
    }
}

struct LineNums<'a> {
    line: &'a str,
    last_end: usize,
}

impl<'a> LineNums<'a> {
    fn new(line: &'a str) -> Self {
        Self { line, last_end: 0 }
    }
}

impl<'a> Iterator for LineNums<'a> {
    type Item = (Range<usize>, u32);

    fn next(&mut self) -> Option<Self::Item> {
        let start = self.line.find(|ch: char| ch.is_ascii_digit())?;
        let actual_start = start + self.last_end;
        let (number, num_str, remaining) = integer(&self.line[start..]).unwrap();
        self.line = remaining;
        self.last_end = actual_start + num_str.len();
        Some((actual_start..self.last_end, number))
    }
}

fn integer(input: &str) -> anyhow::Result<(u32, &str, &str)> {
    let digits = input.chars().take_while(char::is_ascii_digit).count();
    if digits > 0 {
        let int_part = &input[..digits];
        let res = int_part.parse().expect("all characters are digits");
        Ok((res, int_part, &input[digits..]))
    } else {
        Err(anyhow!(r#"expected integer, got "{input}""#))
    }
}

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let mut stdin = std::io::stdin().lock();
    let mut input = String::new();
    let _ = stdin.read_to_string(&mut input);
    let schema = EngineSchema::try_new(&input)?;

    run(
        part,
        |schema| Ok(part1(schema)),
        |schema| Ok(part2(schema)),
        schema,
    )?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = concat!(
        "467..114..\n",
        "...*......\n",
        "..35..633.\n",
        "......#...\n",
        "617*......\n",
        ".....+.58.\n",
        "..592.....\n",
        "......755.\n",
        "...$.*....\n",
        ".664.598..\n",
    );

    #[test]
    fn example_test_part1() {
        let schema = EngineSchema::try_new(TEST_INPUT).unwrap();

        let result = part1(schema);
        assert_eq!(4361, result);
    }

    #[test]
    fn example_test_part2() {
        let schema = EngineSchema::try_new(TEST_INPUT).unwrap();

        let result = part2(schema);
        assert_eq!(467835, result);
    }
}
