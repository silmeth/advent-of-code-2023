use std::time::{Duration, Instant};

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_11::*;

static INPUT: &str = include_str!("input");

fn parsing_benchmark(c: &mut Criterion) {
    c.bench_function("day 11, parsing", |b| {
        b.iter(|| parse(black_box(INPUT)).unwrap())
    });
}

fn perform_benchmark<R: FnMut(Galaxies)>(name: &'static str, mut routine: R, c: &mut Criterion) {
    let input = parse(INPUT).unwrap();
    c.bench_function(name, |b| {
        b.iter_custom(|iters| {
            let mut time = Duration::ZERO;
            for _ in 0..iters {
                let input = input.clone();
                let start = Instant::now();
                routine(input);
                time += start.elapsed();
            }
            time
        })
    });
}

fn part1_benchmark(c: &mut Criterion) {
    perform_benchmark(
        "day 11, part 1",
        |input| assert_eq!(10292708, part1(black_box(input)).unwrap()),
        c,
    )
}

fn part2_benchmark(c: &mut Criterion) {
    perform_benchmark(
        "day 11, part 2",
        |input| assert_eq!(790194712336, part2(black_box(input)).unwrap()),
        c,
    )
}

criterion_group!(benches, parsing_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);
