use std::collections::HashSet;

use anyhow::{anyhow, Context as _};
use itertools::Itertools as _;

#[derive(Debug, Clone)]
pub struct Galaxies {
    galaxies: Vec<(usize, usize)>,
    width: usize,
    height: usize,
}

pub fn part1(galaxies: Galaxies) -> anyhow::Result<u64> {
    distances(galaxies, 2)
}

pub fn part2(galaxies: Galaxies) -> anyhow::Result<u64> {
    distances(galaxies, 1_000_000)
}

pub fn parse(input: &str) -> anyhow::Result<Galaxies> {
    let width = input
        .lines()
        .next()
        .context("expected multiple lines")?
        .len();
    let height = input.len() / (width + 1);
    let galaxies: Vec<(usize, usize)> = input
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            let width_err = line.len() != width;
            line.char_indices().filter_map(move |(x, c)| {
                if width_err {
                    return Some(Err(anyhow!("expected rectangle map")));
                }
                (c == '#').then_some(Ok((x, y)))
            })
        })
        .collect::<anyhow::Result<_>>()?;

    Ok(Galaxies {
        galaxies,
        width,
        height,
    })
}

fn distances(
    Galaxies {
        galaxies,
        width,
        height,
    }: Galaxies,
    expansion_rate: usize,
) -> anyhow::Result<u64> {
    let expanded = expand(galaxies, width, height, expansion_rate);

    let res: isize = expanded
        .iter()
        .combinations(2)
        .map(|pair| {
            isize::abs(pair[1].0 as isize - pair[0].0 as isize)
                + isize::abs(pair[1].1 as isize - pair[0].1 as isize)
        })
        .sum();
    res.try_into().context("result does not fit in u64")
}

fn expand(
    mut galaxies: Vec<(usize, usize)>,
    width: usize,
    height: usize,
    expansion_rate: usize,
) -> Vec<(usize, usize)> {
    let expansion_shift = expansion_rate - 1;
    let xs = galaxies.iter().map(|(x, _)| *x).collect::<HashSet<_>>();
    let ys = galaxies.iter().map(|(_, y)| *y).collect::<HashSet<_>>();

    // how much we’ve shifted the galaxies already in the loop
    let mut shift = 0;
    for x in 0..width {
        if !xs.contains(&x) {
            for galaxy in &mut galaxies {
                if galaxy.0 > x + shift {
                    galaxy.0 += expansion_shift;
                }
            }
            shift += expansion_shift;
        }
    }

    let mut shift = 0;
    for y in 0..height {
        if !ys.contains(&y) {
            // galaxies are laid in rows of increasing y, so they’re effectively sorted by y
            let Err(first) = galaxies.binary_search_by_key(&(y + shift), |(_, galaxy_y)| *galaxy_y)
            else {
                unreachable!("needle guaranteed to not be in haystack")
            };
            for galaxy in &mut galaxies[first..] {
                galaxy.1 += expansion_shift;
            }
            shift += expansion_shift;
        }
    }

    galaxies
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "...#......\n",
        ".......#..\n",
        "#.........\n",
        "..........\n",
        "......#...\n",
        ".#........\n",
        ".........#\n",
        "..........\n",
        ".......#..\n",
        "#...#.....\n",
    );

    #[test]
    fn example_input_part1() {
        let galaxies = parse(INPUT).unwrap();
        let result = part1(galaxies).unwrap();
        assert_eq!(374, result);
    }

    #[test]
    fn example_input_10x_expansion_rate() {
        let galaxies = parse(INPUT).unwrap();
        let result = distances(galaxies, 10).unwrap();
        assert_eq!(1030, result);
    }

    #[test]
    fn example_input_100x_expansion_rate() {
        let galaxies = parse(INPUT).unwrap();
        let result = distances(galaxies, 100).unwrap();
        assert_eq!(8410, result);
    }
}
