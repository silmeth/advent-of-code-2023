use day_11::{parse, part1, part2};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let galaxies = parse(utils::read_entire_stdin()?.as_str())?;

    run(part, part1, part2, galaxies)
}
