use day_21::{part1, part2, GardenMap};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let map: GardenMap = utils::read_entire_stdin()?.parse()?;

    run(
        part,
        |map| Ok(part1(map, 64)),
        |map| part2(map, 26501365),
        &map,
    )
}
