use std::{collections::HashMap, str::FromStr};

use anyhow::{anyhow, Context as _};

// based on code copied from day 17
pub struct GardenMap {
    costs: Vec<Tile>,
    width: isize,
    height: isize,
    start_x: isize,
    start_y: isize,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Tile {
    Ground,
    Rock,
}

impl std::ops::Index<(isize, isize)> for GardenMap {
    type Output = Tile;

    fn index(&self, (x, y): (isize, isize)) -> &Self::Output {
        let x = isize::rem_euclid(x, self.width) as usize;
        let y = (isize::rem_euclid(y, self.height) * self.width) as usize;
        &self.costs[y + x]
    }
}

impl FromStr for GardenMap {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut width: Option<isize> = None;
        let mut height: isize = 0;
        let mut start_pos: Option<(isize, isize)> = None;

        let costs = s
            .as_bytes()
            .iter()
            .enumerate()
            .filter_map(|(idx, b)| match b {
                b'.' => Some(Ok(Tile::Ground)),
                b'#' => Some(Ok(Tile::Rock)),
                b'S' => {
                    start_pos = if let Some(w) = width {
                        Some((idx as isize % (w + 1), idx as isize / (w + 1)))
                    } else {
                        Some((idx as isize, 0))
                    };
                    Some(Ok(Tile::Ground))
                }
                b'\n' => {
                    if let Some(width) = width {
                        if (idx as isize + 1) % (width + 1) != 0 {
                            Some(Err(anyhow!("expected all lines to be of equal length")))
                        } else {
                            height += 1;
                            None
                        }
                    } else {
                        height += 1;
                        width = Some(idx as isize);
                        None
                    }
                }
                _ => Some(
                    std::str::from_utf8(&[*b])
                        .with_context(|| format!("non-ASCII byte {}", *b))
                        .and_then(|character| Err(anyhow!("unexpected character '{}'", character))),
                ),
            })
            .collect::<anyhow::Result<_>>()?;

        let (start_x, start_y) = start_pos.context("starting position not found")?;

        Ok(Self {
            costs,
            width: width.with_context(|| format!("expected at least one line, got {s}"))?,
            height,
            start_x,
            start_y,
        })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Node {
    x: isize,
    y: isize,
    distance: usize,
}

impl Node {
    fn neighbours(&self) -> impl Iterator<Item = (isize, isize)> + '_ {
        [Direction::N, Direction::S, Direction::E, Direction::W]
            .into_iter()
            .map(|dir| dir.move_coords(self.x, self.y))
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum Direction {
    N,
    W,
    E,
    S,
}

impl Direction {
    /// let’s just wrap the coords and not worry about isize::MAX-sized maps…
    pub fn move_coords(self, x: isize, y: isize) -> (isize, isize) {
        match self {
            Self::N => (x, y.wrapping_sub(1)),
            Self::E => (x.wrapping_add(1), y),
            Self::S => (x, y.wrapping_add(1)),
            Self::W => (x.wrapping_sub(1), y),
        }
    }
}

pub fn dijkstra_reachable<F, G>(
    map: &GardenMap,
    mut filter_neighbours: F,
    mut stop: G,
) -> impl Iterator<Item = Node>
where
    for<'a, 'b> F: FnMut(&'a Node, &'b (isize, isize)) -> bool,
    for<'a> G: FnMut(&'a Node) -> bool,
{
    let mut visited: HashMap<(isize, isize), Node> = HashMap::new();
    let mut candidates: HashMap<(isize, isize), Node> = HashMap::new();
    let mut current = Node {
        x: map.start_x,
        y: map.start_y,
        distance: 0,
    };

    while !stop(&current) {
        for (x, y) in current
            .neighbours()
            .filter(|(x, y)| !visited.contains_key(&(*x, *y)) && map[(*x, *y)] == Tile::Ground)
            .filter(|n| filter_neighbours(&current, n))
        {
            let distance = current.distance + 1;

            let candidate_entry = candidates.entry((x, y)).or_insert(Node { x, y, distance });
            candidate_entry.distance = usize::min(distance, candidate_entry.distance);
        }

        visited.insert((current.x, current.y), current);
        let next = *candidates
            .iter()
            .min_by_key(|(_, node)| node.distance)
            .map(|(k, _)| k)
            .unwrap();
        current = candidates.remove(&next).unwrap();
    }

    visited.into_values()
}

pub fn part1(map: &GardenMap, max_distance: usize) -> usize {
    dijkstra_reachable(
        map,
        #[inline]
        |_, _| true,
        |n| n.distance > max_distance,
    )
    // count no. of nodes reachable in max_distance *and* with the same parity
    .filter(|n| n.distance <= max_distance && (n.distance % 2 == max_distance % 2))
    .count()
}

pub fn part2(map: &GardenMap, max_distance: usize) -> anyhow::Result<usize> {
    // let’s assume the map is always perfectly square
    let map_size = if map.width == map.height {
        usize::try_from(map.width).context("map size out of bounds")?
    } else {
        return Err(anyhow!("part 2 requires perfectly square map"));
    };

    let remainder = max_distance % map_size;
    let n = max_distance / map_size;

    // Let’s calculate three points – all steps reachable in the step limits:
    // 1. `0 + rem`,
    // 2. `1 * map_size + rem`,
    // 3. and `2 * map_size + rem`,
    // and since the area grows quadratically with distance, let’s assume that those grow
    // quadratically too. With this assumption let’s extrapolate to `n * map_size + rem`.
    //
    // This does *not* work for the example data, it does on real input.

    let zeroth = part1(map, remainder);
    let first = part1(map, map_size + remainder);
    let second = part1(map, 2 * map_size + remainder);

    // Now let’s use the method of differences to compute the value at n * map_size + remainder.
    let mut last_value = second;
    let mut last_first_diff = second - first;
    let second_diff = last_first_diff - (first - zeroth);

    // we begin with i = 3, and iterate until i = n
    for _ in 3..=n {
        last_first_diff += second_diff;
        last_value += last_first_diff;
    }

    Ok(last_value)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "...........\n",
        ".....###.#.\n",
        ".###.##..#.\n",
        "..#.#...#..\n",
        "....#.#....\n",
        ".##..S####.\n",
        ".##..#...#.\n",
        ".......##..\n",
        ".##.#.####.\n",
        ".##..##.##.\n",
        "...........\n",
    );

    #[test]
    fn test_part1() {
        let map = INPUT.parse().unwrap();
        let res = part1(&map, 6);
        assert_eq!(16, res);
    }

    #[test]
    fn test_part1_infinite_map() {
        let map = INPUT.parse().unwrap();
        let res = part1(&map, 100);
        assert_eq!(6536, res);
    }

    //#[test]
    //fn test_part2() {
    //    let map = INPUT.parse().unwrap();
    //    let res = part2(&map, 5000).unwrap();
    //    assert_eq!(16733044, res);
    //}
}
