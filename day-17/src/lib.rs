use std::{
    collections::{HashMap, HashSet},
    str::FromStr,
};

use anyhow::{anyhow, Context as _};
use utils::ascii_digit_to_num;

pub struct CityMap {
    costs: Vec<u8>,
    width: usize,
    height: usize,
}

impl CityMap {
    #[inline]
    fn coords_within_map(&self, x: usize, y: usize) -> bool {
        x < self.width && y < self.height
    }
}

impl std::ops::Index<(usize, usize)> for CityMap {
    type Output = u8;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        assert!(self.coords_within_map(x, y));

        &self.costs[y * self.width + x]
    }
}

impl FromStr for CityMap {
    type Err = anyhow::Error;

    // let’s parse it byte by byte this time
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut width = None;
        let mut height = 0;

        let costs = s
            .as_bytes()
            .iter()
            .enumerate()
            .filter_map(|(idx, b)| match b {
                b'0'..=b'9' => Some(Ok(ascii_digit_to_num(*b))),
                b'\n' => {
                    if let Some(width) = width {
                        if (idx + 1) % (width + 1) != 0 {
                            Some(Err(anyhow!("expected all lines to be of equal length")))
                        } else {
                            height += 1;
                            None
                        }
                    } else {
                        height += 1;
                        width = Some(idx);
                        None
                    }
                }
                _ => Some(
                    std::str::from_utf8(&[*b])
                        .with_context(|| format!("non-ASCII byte {}", *b))
                        .and_then(|character| Err(anyhow!("unexpected character '{}'", character))),
                ),
            })
            .collect::<anyhow::Result<_>>()?;

        Ok(Self {
            costs,
            width: width.with_context(|| format!("expected at least one line, got {s}"))?,
            height,
        })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Node {
    x: usize,
    y: usize,
    straight: StraightLine,
    heat_loss: usize,
}

impl Node {
    fn neighbours(&self) -> impl Iterator<Item = (StraightLine, usize, usize)> + '_ {
        [Direction::N, Direction::S, Direction::E, Direction::W]
            .into_iter()
            // filter out turn-arounds
            .filter(|dir| {
                !matches!(
                    (dir, self.straight.0),
                    (Direction::N, Direction::S)
                        | (Direction::S, Direction::N)
                        | (Direction::E, Direction::W)
                        | (Direction::W, Direction::E)
                )
            })
            .map(|dir| {
                let (x, y) = dir.move_coords(self.x, self.y);
                let line = if self.straight.0 == dir {
                    StraightLine(dir, self.straight.1 + 1)
                } else {
                    StraightLine(dir, 1)
                };
                (line, x, y)
            })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct StraightLine(Direction, usize);

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum Direction {
    N,
    W,
    E,
    S,
}

impl Direction {
    /// let’s just wrap the coords and not worry about usize::MAX-sized maps…
    pub fn move_coords(self, x: usize, y: usize) -> (usize, usize) {
        match self {
            Self::N => (x, y.wrapping_sub(1)),
            Self::E => (x.wrapping_add(1), y),
            Self::S => (x, y.wrapping_add(1)),
            Self::W => (x.wrapping_sub(1), y),
        }
    }
}

pub fn dijkstra_distance<F, G>(map: &CityMap, mut filter_neighbours: F, mut stop: G) -> usize
where
    for<'a, 'b> F: FnMut(&'a Node, &'b (StraightLine, usize, usize)) -> bool,
    for<'a> G: FnMut(&'a Node) -> bool,
{
    let mut visited: HashSet<(usize, usize, StraightLine)> = HashSet::new();
    let mut candidates: HashMap<(usize, usize, StraightLine), Node> = HashMap::new();
    let mut current = Node {
        x: 0,
        y: 0,
        // direction here doesn’t matter
        straight: StraightLine(Direction::E, 0),
        heat_loss: 0,
    };

    while !stop(&current) {
        for (line, x, y) in current
            .neighbours()
            .filter(|(line, x, y)| {
                map.coords_within_map(*x, *y) && !visited.contains(&(*x, *y, *line))
            })
            .filter(|n| filter_neighbours(&current, n))
        {
            let heat_loss = current.heat_loss + usize::from(map[(x, y)]);

            let candidate_entry = candidates.entry((x, y, line)).or_insert(Node {
                x,
                y,
                straight: line,
                heat_loss,
            });
            candidate_entry.heat_loss = usize::min(heat_loss, candidate_entry.heat_loss);
        }

        visited.insert((current.x, current.y, current.straight));
        let next = *candidates
            .iter()
            .min_by_key(|(_, node)| node.heat_loss)
            .map(|(k, _)| k)
            .unwrap();
        current = candidates.remove(&next).unwrap();
    }

    current.heat_loss
}

pub fn part1(map: &CityMap) -> usize {
    dijkstra_distance(
        map,
        |_, (line, _, _)| line.1 <= 3,
        |node| node.x == map.width - 1 && node.y == map.height - 1,
    )
}

pub fn part2(map: &CityMap) -> usize {
    dijkstra_distance(
        map,
        |node, (line, _, _)| {
            if node.straight.1 == 0 {
                // just initialized – so just let the algorithm start;
                // all later lines have at least 1 as this value
                true
            } else if node.straight.1 < 4 {
                // before 4 tiles it must keep direction
                line.0 == node.straight.0
            } else {
                line.1 <= 10
            }
        },
        |node| node.x == map.width - 1 && node.y == map.height - 1 && node.straight.1 >= 4,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "2413432311323\n",
        "3215453535623\n",
        "3255245654254\n",
        "3446585845452\n",
        "4546657867536\n",
        "1438598798454\n",
        "4457876987766\n",
        "3637877979653\n",
        "4654967986887\n",
        "4564679986453\n",
        "1224686865563\n",
        "2546548887735\n",
        "4322674655533\n",
    );

    #[test]
    fn test_parsing() {
        let result: CityMap = INPUT.parse().unwrap();
        assert_eq!((13, 13), (result.width, result.height));
    }

    #[test]
    fn test_part1() {
        let map: CityMap = INPUT.parse().unwrap();
        assert_eq!(102, part1(&map));
    }

    #[test]
    fn test_part2() {
        let map: CityMap = INPUT.parse().unwrap();
        assert_eq!(94, part2(&map));
    }
}
