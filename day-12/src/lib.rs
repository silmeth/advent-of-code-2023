use anyhow::{anyhow, Context as _};
use cached::{Cached as _, SizedCache};
use rayon::{
    iter::{IntoParallelRefIterator, ParallelIterator},
    str::ParallelString,
};
use std::{fmt::Debug, io::Write};
use yoke::{Yoke, Yokeable};

#[derive(Yokeable, Debug)]
pub struct Record<'a> {
    groups: Vec<usize>,
    pattern: &'a [u8],
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Placement {
    at: usize,
    size: usize,
}

pub fn parse(input: &'_ str) -> anyhow::Result<Vec<Record<'_>>> {
    input
        .par_lines()
        .map(|line| {
            let (pattern, groups) = line
                .split_once(' ')
                .with_context(|| format!(r#"expected pair "<pattern> <groups>", got "{line}""#))?;
            if pattern.chars().any(|c| !['.', '#', '?'].contains(&c)) {
                return Err(anyhow!(
                    r#"pattern can only contain the characters '.', '#', '?', got "{}""#,
                    pattern
                ));
            }
            let pattern = pattern.as_bytes();
            let groups = groups
                .split(',')
                .map(|group| {
                    group
                        .parse()
                        .with_context(|| format!("expected int, got {group}"))
                })
                .collect::<anyhow::Result<_>>()?;

            Ok(Record { groups, pattern })
        })
        .collect()
}

pub fn part1(records: &[Record<'_>]) -> anyhow::Result<u64> {
    sum_counts_for_all_records(records.par_iter())
}

pub fn part2(records: &[Record<'_>]) -> anyhow::Result<u64> {
    let unfolded = unfold(records);
    sum_counts_for_all_records(unfolded.par_iter().map(Yoke::get))
}

pub fn sum_counts_for_all_records<'a, I>(records: I) -> anyhow::Result<u64>
where
    I: ParallelIterator<Item = &'a Record<'a>> + 'a,
{
    Ok(records.map(CachingCounter::count_all_arrangements).sum())
}

pub fn unfold(records: &[Record<'_>]) -> Vec<Yoke<Record<'static>, Vec<u8>>> {
    records
        .par_iter()
        .map(|record| {
            let mut pattern = Vec::with_capacity(record.pattern.len() * 5 + 4);
            for i in 0..5 {
                if i > 0 {
                    pattern.write_all(b"?").unwrap();
                }
                pattern.write_all(record.pattern).unwrap();
            }
            let mut groups = Vec::with_capacity(record.groups.len() * 5);
            for _ in 0..5 {
                groups.extend(record.groups.iter().cloned());
            }

            Yoke::attach_to_cart(pattern, move |pattern| Record { pattern, groups })
        })
        .collect()
}

struct CachingCounter<'a> {
    cache: SizedCache<(usize, usize, usize), u64>,
    pattern: &'a [u8],
}

impl<'a> CachingCounter<'a> {
    fn count_all_arrangements(record: &'a Record) -> u64 {
        let mut cache = Self::new(record.pattern);
        let groups = &record.groups[..];
        let springs_to_cover = record.pattern.iter().filter(|c| **c == b'#').count();
        cache.count_recursive(groups, 0, springs_to_cover)
    }

    fn new(pattern: &'a [u8]) -> Self {
        Self {
            cache: SizedCache::with_size(256),
            pattern,
        }
    }

    fn get_or_cache(&mut self, groups: &[usize], start: usize, springs_to_cover: usize) -> u64 {
        let key = (groups.len(), start, springs_to_cover);
        let value = self.cache.cache_get(&key).copied();
        if let Some(value) = value {
            value
        } else {
            let value = self.count_recursive(groups, start, springs_to_cover);
            self.cache.cache_set(key, value);
            value
        }
    }

    fn count_recursive(&mut self, groups: &[usize], start: usize, springs_to_cover: usize) -> u64 {
        match groups {
            [] => {
                if springs_to_cover == 0 {
                    1
                } else {
                    0
                }
            }
            [size, rest @ ..] => {
                let mut result = 0;
                // minimum pattern length required to fit remaining groups – no reason to allocate
                // the first group at the end of the input if we know the rest won’t fit
                let min_rest_length = rest.iter().sum::<usize>() + rest.len();
                if self.pattern.len() < start + size + min_rest_length {
                    return 0;
                }
                let end = self.pattern.len() - size - min_rest_length;
                for i in start..=end {
                    // if we skipped a '#' – no good, don’t iterate more
                    if i > start && self.pattern[start..i].iter().any(|c| *c == b'#') {
                        break;
                    }

                    if self.pattern[i..i + size]
                        .iter()
                        .all(|c| *c == b'#' || *c == b'?')
                        && (i == end || self.pattern[i + size] != b'#')
                        && (i == 0 || self.pattern[i - 1] != b'#')
                    {
                        let springs_remaining = springs_to_cover
                            - self.pattern[i..i + size]
                                .iter()
                                .filter(|c| **c == b'#')
                                .count();
                        result += self.get_or_cache(rest, i + size + 1, springs_remaining);
                    }
                }
                result
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = concat!(
        "???.### 1,1,3\n",
        ".??..??...?##. 1,1,3\n",
        "?#?#?#?#?#?#?#? 1,3,1,6\n",
        "????.#...#... 4,1,1\n",
        "????.######..#####. 1,6,5\n",
        "?###???????? 3,2,1\n",
    );

    #[test]
    fn test_part1() {
        let input = parse(INPUT).unwrap();
        let res = part1(&input).unwrap();

        assert_eq!(21, res);
    }

    #[test]
    fn test_every_hash_is_used_part1() {
        let input = parse("?###????? 3\n").unwrap();
        let res = part1(&input).unwrap();

        assert_eq!(1, res);
    }

    #[test]
    fn test_allocation_if_first_fit_does_not_work() {
        let input = parse("??#.????#???.#?# 1,5,1,1,1\n").unwrap();
        let res = part1(&input).unwrap();

        assert_eq!(3, res);
    }

    #[test]
    fn test_part2() {
        let input = parse(INPUT).unwrap();
        let res = part2(&input).unwrap();

        assert_eq!(525_152, res);
    }
}
