use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_12::*;
use rayon::iter::{IntoParallelRefIterator, ParallelIterator as _};

static INPUT: &str = include_str!("input");

fn parsing_benchmark(c: &mut Criterion) {
    c.bench_function("day 12, parsing", |b| {
        b.iter(|| parse(black_box(INPUT)).unwrap())
    });
}

fn part1_benchmark(c: &mut Criterion) {
    let input = parse(INPUT).unwrap();
    c.bench_function("day 12, part 1", |b| {
        b.iter(|| assert_eq!(7169, part1(black_box(&input)).unwrap()))
    });
}

fn unfolding_benchmark(c: &mut Criterion) {
    let input = parse(INPUT).unwrap();
    c.bench_function("day 12, unfolding", |b| {
        b.iter(|| black_box(unfold(black_box(&input))))
    });
}

fn part2_benchmark(c: &mut Criterion) {
    let input = parse(INPUT).unwrap();
    let input = unfold(&input);
    c.bench_function("day 12, part 2 (count unfolded)", |b| {
        b.iter(|| {
            assert_eq!(
                1738259948652,
                sum_counts_for_all_records(black_box(&input).par_iter().map(yoke::Yoke::get))
                    .unwrap()
            )
        })
    });
}

criterion_group!(
    benches,
    parsing_benchmark,
    part1_benchmark,
    unfolding_benchmark,
    part2_benchmark
);
criterion_main!(benches);
