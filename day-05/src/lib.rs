use std::str::FromStr;

use anyhow::anyhow;
use anyhow::Context as _;
use utils::expect;

type ValueRange = std::ops::Range<u64>;

pub fn part1(almanac: &Almanac) -> anyhow::Result<u64> {
    almanac
        .seeds
        .iter()
        .map(|idx| almanac.plot(*idx))
        .min()
        .context("no seeds found")
}

pub fn part2(almanac: &Almanac) -> anyhow::Result<u64> {
    almanac
        .seeds
        .chunks_exact(2)
        .map(|chunk| {
            let start = chunk[0];
            let len = chunk[1];
            almanac.smallest_plot_in_range(start..start + len)
        })
        .min()
        .context("no seed found")
}

pub struct Almanac {
    seeds: Vec<u64>,
    seed_to_soil: AlmanacMap,
    soil_to_fertilizer: AlmanacMap,
    fertilizer_to_water: AlmanacMap,
    water_to_light: AlmanacMap,
    light_to_temperature: AlmanacMap,
    temperature_to_humidity: AlmanacMap,
    humidity_to_location: AlmanacMap,
}

impl Almanac {
    pub fn plot(&self, idx: u64) -> u64 {
        let idx = self.seed_to_soil.get(idx);
        let idx = self.soil_to_fertilizer.get(idx);
        let idx = self.fertilizer_to_water.get(idx);
        let idx = self.water_to_light.get(idx);
        let idx = self.light_to_temperature.get(idx);
        let idx = self.temperature_to_humidity.get(idx);
        self.humidity_to_location.get(idx)
    }

    pub fn smallest_plot_in_range(&self, range: ValueRange) -> u64 {
        self.seed_to_soil
            .ranges(range)
            .into_iter()
            .flat_map(|range| self.soil_to_fertilizer.ranges(range).into_iter())
            .flat_map(|range| self.fertilizer_to_water.ranges(range).into_iter())
            .flat_map(|range| self.water_to_light.ranges(range).into_iter())
            .flat_map(|range| self.light_to_temperature.ranges(range).into_iter())
            .flat_map(|range| self.temperature_to_humidity.ranges(range).into_iter())
            .flat_map(|range| self.humidity_to_location.ranges(range).into_iter())
            .map(|range| range.start)
            .min()
            .expect("almanac has minimal value")
    }

    fn seeds(input: &str) -> anyhow::Result<(Vec<u64>, &str)> {
        let (seeds, remaining) = input
            .split_once("\r\n")
            .or_else(|| input.split_once('\n'))
            .context("expected multi-line input")?;
        let seeds = seeds
            .split(' ')
            .map(str::parse::<u64>)
            .map(|res| res.context("failed to parse number"))
            .collect::<anyhow::Result<_>>()
            .context("failed to parse seeds")?;
        Ok((seeds, remaining.trim_start()))
    }
}

impl FromStr for Almanac {
    type Err = anyhow::Error;

    fn from_str(input: &str) -> anyhow::Result<Self> {
        let mut seed_to_soil = None;
        let mut soil_to_fertilizer = None;
        let mut fertilizer_to_water = None;
        let mut water_to_light = None;
        let mut light_to_temperature = None;
        let mut temperature_to_humidity = None;
        let mut humidity_to_location = None;

        let remaining = expect("seeds: ", input)?;

        let (seeds, remaining) = Self::seeds(remaining)?;

        for almanac_map in remaining.split("\r\n\r\n").flat_map(|it| it.split("\n\n")) {
            let mut lines = almanac_map.lines();
            let name = lines
                .next()
                .context("")?
                .strip_suffix(" map:")
                .context(r#"expected map name followed by "map:""#)?;
            let mut entries: Vec<_> = lines
                .map(str::parse::<AlmanacMapEntry>)
                .collect::<anyhow::Result<_>>()?;
            entries.sort_by_key(|entry| entry.input_range.start);
            let map = AlmanacMap(entries);

            match name {
                "seed-to-soil" => seed_to_soil = Some(map),
                "soil-to-fertilizer" => soil_to_fertilizer = Some(map),
                "fertilizer-to-water" => fertilizer_to_water = Some(map),
                "water-to-light" => water_to_light = Some(map),
                "light-to-temperature" => light_to_temperature = Some(map),
                "temperature-to-humidity" => temperature_to_humidity = Some(map),
                "humidity-to-location" => humidity_to_location = Some(map),
                _ => return Err(anyhow!("unknown map {name}")),
            }
        }

        Ok(Self {
            seeds,
            seed_to_soil: seed_to_soil.context("seed-to-soil map missing")?,
            soil_to_fertilizer: soil_to_fertilizer.context("soil-to-fertilizer map missing")?,
            fertilizer_to_water: fertilizer_to_water.context("fertilizer-to-water map missing")?,
            water_to_light: water_to_light.context("water-to-light map missing")?,
            light_to_temperature: light_to_temperature
                .context("light-to-temperature map missing")?,
            temperature_to_humidity: temperature_to_humidity
                .context("temperature-to-humidity map missing")?,
            humidity_to_location: humidity_to_location
                .context("humidity-to-location map missing")?,
        })
    }
}

struct AlmanacMap(Vec<AlmanacMapEntry>);

impl AlmanacMap {
    fn get(&self, idx: u64) -> u64 {
        if let Ok(entry_idx) = self.0.binary_search_by(|probe| {
            match (
                probe.input_range.start.cmp(&idx),
                probe.input_range.end.cmp(&idx),
            ) {
                (std::cmp::Ordering::Less, std::cmp::Ordering::Greater)
                | (std::cmp::Ordering::Equal, _) => std::cmp::Ordering::Equal,
                (_, std::cmp::Ordering::Less) | (_, std::cmp::Ordering::Equal) => {
                    std::cmp::Ordering::Less
                }
                (std::cmp::Ordering::Greater, _) => std::cmp::Ordering::Greater,
            }
        }) {
            self.0[entry_idx].offset(idx)
        } else {
            idx
        }
    }

    fn ranges(&self, range: ValueRange) -> Vec<ValueRange> {
        let (start, end) = (range.start, range.end);
        let (mut ranges, remaining) =
            self.0
                .iter()
                .fold((Vec::new(), start), |(mut acc, mut left), entry| {
                    // values before the current range
                    if left < entry.input_range.start {
                        // clip the end of this range to the beginning of the current entry
                        let cur_end = u64::min(entry.input_range.start, end);
                        // if non-empty
                        if left < cur_end {
                            acc.push(left..cur_end);
                            // and move the beginning to the right
                            left = cur_end;
                        }
                    }

                    // values in the current range
                    if entry.input_range.start <= left && left < end {
                        // move the end to the end of the current entry
                        let cur_end = u64::min(entry.input_range.end, end);
                        if left < cur_end {
                            let res = entry.offset(left)..entry.offset(cur_end - 1) + 1;
                            acc.push(res);
                            left = cur_end;
                        }
                    }
                    (acc, left)
                });

        if remaining < end {
            ranges.push(remaining..end);
        }

        ranges
    }
}

#[derive(Debug, Clone)]
struct AlmanacMapEntry {
    input_range: ValueRange,
    offset: i64,
}

impl AlmanacMapEntry {
    fn offset(&self, idx: u64) -> u64 {
        (i64::try_from(idx).expect("idx out of bound") + self.offset)
            .try_into()
            .expect("almanac value out of bound")
    }
}

impl FromStr for AlmanacMapEntry {
    type Err = anyhow::Error;

    fn from_str(input: &str) -> anyhow::Result<Self> {
        let mut nums = input.split(' ').map(str::parse::<u64>);
        let dest = nums
            .next()
            .context("dest missing")?
            .context("failed to parse dest")?;
        let start = nums
            .next()
            .context("start missing")?
            .context("failed to parse range start")?;
        let len = nums
            .next()
            .context("len missing")?
            .context("failed to parse len")?;

        Ok(Self {
            input_range: start..start + len,
            offset: i64::try_from(dest).expect("dest out of bound")
                - i64::try_from(start).expect("start out of bound"),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = concat!(
        "seeds: 79 14 55 13\n",
        "\n",
        "seed-to-soil map:\n",
        "50 98 2\n",
        "52 50 48\n",
        "\n",
        "soil-to-fertilizer map:\n",
        "0 15 37\n",
        "37 52 2\n",
        "39 0 15\n",
        "\n",
        "fertilizer-to-water map:\n",
        "49 53 8\n",
        "0 11 42\n",
        "42 0 7\n",
        "57 7 4\n",
        "\n",
        "water-to-light map:\n",
        "88 18 7\n",
        "18 25 70\n",
        "\n",
        "light-to-temperature map:\n",
        "45 77 23\n",
        "81 45 19\n",
        "68 64 13\n",
        "\n",
        "temperature-to-humidity map:\n",
        "0 69 1\n",
        "1 0 69\n",
        "\n",
        "humidity-to-location map:\n",
        "60 56 37\n",
        "56 93 4\n",
    );

    #[test]
    fn example_input_part1() {
        let result = part1(&TEST_INPUT.parse().unwrap()).unwrap();
        assert_eq!(35, result);
    }

    #[test]
    fn example_input_part2() {
        let result = part2(&TEST_INPUT.parse().unwrap()).unwrap();
        assert_eq!(46, result);
    }
}
