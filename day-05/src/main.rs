use day_05::{part1, part2};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let almanac = utils::read_entire_stdin()?.parse()?;

    run(part, part1, part2, &almanac)
}
