use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_05::*;

static INPUT: &str = include_str!("input");

fn parsing_benchmark(c: &mut Criterion) {
    c.bench_function("day 5, parsing", |b| {
        b.iter(|| black_box(INPUT).parse::<Almanac>().unwrap())
    });
}

fn part1_benchmark(c: &mut Criterion) {
    let input = INPUT.parse().unwrap();
    c.bench_function("day 5, part 1", |b| {
        b.iter(|| assert_eq!(214922730, part1(black_box(&input)).unwrap()))
    });
}

fn part2_benchmark(c: &mut Criterion) {
    let input = INPUT.parse().unwrap();
    c.bench_function("day 5, part 2", |b| {
        b.iter(|| assert_eq!(148041808, part2(black_box(&input)).unwrap()))
    });
}

criterion_group!(benches, parsing_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);
