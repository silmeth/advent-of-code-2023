use criterion::{black_box, criterion_group, criterion_main, Criterion};
use day_15::*;

static INPUT: &str = include_str!("input");

fn part1_benchmark(c: &mut Criterion) {
    c.bench_function("day 15, part 1", |b| {
        b.iter(|| assert_eq!(517015, part1(black_box(INPUT))))
    });
}

fn part2_benchmark(c: &mut Criterion) {
    c.bench_function("day 15, part 2", |b| {
        b.iter(|| assert_eq!(286104, part2(black_box(INPUT)).unwrap()))
    });
}

criterion_group!(benches, part1_benchmark, part2_benchmark);
criterion_main!(benches);
