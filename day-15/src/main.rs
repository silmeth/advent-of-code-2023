use day_15::{part1, part2};
use utils::run;

fn main() -> anyhow::Result<()> {
    let part = std::env::args().try_into()?;

    let input = utils::read_entire_stdin()?;

    run(part, |input| Ok(part1(input)), part2, &input)
}
