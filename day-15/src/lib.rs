use std::hash::{BuildHasher, Hasher};

use anyhow::Context;
use smallvec::SmallVec;

type Bucket<'a> = SmallVec<[(&'a str, u8); 8]>;
pub struct HolidayAsciiStringHelperManualArrangementProcedure<
    'a,
    S = HolidayAsciiStringHelperBuilder,
> {
    buckets: Box<[Bucket<'a>; 256]>,
    hash_builder: S,
}

impl<'a, S: BuildHasher> HolidayAsciiStringHelperManualArrangementProcedure<'a, S> {
    pub fn new(hash_builder: S) -> Self {
        Self {
            buckets: Box::new(std::array::from_fn(|_| SmallVec::new())),
            hash_builder,
        }
    }

    pub fn execute_instruction(&mut self, ins: Instruction<'a>) {
        match ins {
            Instruction::Set { key, value } => self.set(key, value),
            Instruction::Remove { key } => self.remove(key),
        }
    }

    fn set(&mut self, key: &'a str, value: u8) {
        let bucket = self.get_box_mut(self.box_id(key));
        match bucket.iter_mut().find(|(k, _)| *k == key) {
            Some(slot) => slot.1 = value,
            None => bucket.push((key, value)),
        }
    }

    #[inline]
    fn get_box(&self, box_id: u8) -> &[(&'a str, u8)] {
        &self.buckets[usize::from(box_id)]
    }

    fn remove(&mut self, key: &str) {
        let bucket = self.get_box_mut(self.box_id(key));
        let idx = bucket.iter().position(|(k, _)| *k == key);
        if let Some(idx) = idx {
            let _ = bucket.remove(idx);
        }
    }

    #[inline]
    fn get_box_mut(&mut self, box_id: u8) -> &mut SmallVec<[(&'a str, u8); 8]> {
        &mut self.buckets[usize::from(box_id)]
    }

    #[inline]
    fn box_id(&self, key: &str) -> u8 {
        let mut hasher = self.hash_builder.build_hasher();
        hasher.write(key.as_bytes());
        (hasher.finish() & 0xff) as u8
    }
}
impl Default for HolidayAsciiStringHelperManualArrangementProcedure<'_> {
    fn default() -> Self {
        Self::new(HolidayAsciiStringHelperBuilder)
    }
}
impl std::fmt::Display for HolidayAsciiStringHelperManualArrangementProcedure<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for i in 0..=255 {
            let bucket = self.get_box(i);
            if !bucket.is_empty() {
                write!(f, "{i}: {bucket:?}")?;
            }
        }
        Ok(())
    }
}

pub enum Instruction<'a> {
    Set { key: &'a str, value: u8 },
    Remove { key: &'a str },
}

impl<'a> Instruction<'a> {
    pub fn try_from_str(s: &'a str) -> anyhow::Result<Self> {
        let create_err = || format!("expected instruction, got {s}");
        let idx = s.find(['-', '=']).with_context(create_err)?;
        match &s[idx..idx + 1] {
            "-" => Ok(Self::Remove { key: &s[0..idx] }),
            "=" => Ok(Self::Set {
                key: &s[0..idx],
                value: s
                    .get(idx + 1..)
                    .with_context(create_err)?
                    .parse()
                    .with_context(create_err)?,
            }),
            _ => unreachable!(),
        }
    }
}

pub struct HolidayAsciiStringHelper {
    state: u64,
}
pub struct HolidayAsciiStringHelperBuilder;
impl BuildHasher for HolidayAsciiStringHelperBuilder {
    type Hasher = HolidayAsciiStringHelper;

    fn build_hasher(&self) -> Self::Hasher {
        Self::Hasher { state: 0 }
    }
}

impl HolidayAsciiStringHelper {
    pub fn hash(s: &str) -> u8 {
        let mut state = Self { state: 0 };
        state.write(s.as_bytes());
        state.finish() as u8
    }
}

impl Hasher for HolidayAsciiStringHelper {
    fn write_u8(&mut self, byte: u8) {
        let byte = u64::from(byte);
        self.state = ((self.state + byte) * 17) & 0xff;
    }

    fn finish(&self) -> u64 {
        self.state
    }

    fn write(&mut self, bytes: &[u8]) {
        for b in bytes {
            self.write_u8(*b);
        }
    }
}

pub fn part1(input: &str) -> usize {
    input
        .trim()
        .split(',')
        .map(HolidayAsciiStringHelper::hash)
        .map(usize::from)
        .sum::<usize>()
}

pub fn part2(input: &str) -> anyhow::Result<usize> {
    let mut hashmap = HolidayAsciiStringHelperManualArrangementProcedure::default();
    for ins in input.trim().split(',').map(Instruction::try_from_str) {
        hashmap.execute_instruction(ins?);
    }

    Ok(hashmap
        .buckets
        .iter()
        .enumerate()
        .map(|(box_id, box_)| {
            box_.iter()
                .enumerate()
                .map(|(slot_id, (_, value))| (box_id + 1) * (slot_id + 1) * usize::from(*value))
                .sum::<usize>()
        })
        .sum::<usize>())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash() {
        assert_eq!(52, HolidayAsciiStringHelper::hash("HASH"));
    }

    #[test]
    fn test_part1() {
        assert_eq!(
            1320,
            part1("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7\n")
        );
    }

    #[test]
    fn test_part2() {
        assert_eq!(
            145,
            part2("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7\n").unwrap(),
        );
    }
}
